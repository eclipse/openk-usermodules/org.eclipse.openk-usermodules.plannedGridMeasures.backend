To: test1Recipienttest.de, test2Recipient@test.de
CC: testCCRecipient@test.de, test2CCRecipient@test.de
Subject: Die Maßnahme $gridMeasureTitle$ mit Beginn: $plannedStarttimeFirstSinglemeasure$ wurde in den Status Beantragt geändert.

Body:
TEST
Sehr geehrte Damen und Herren,

die im Betreff genannte Maßnahme ist über folgenden Link erreichbar:

$directMeasureLink$

Mit freundlichen Grüßen

Ihre Admin-Meister-Team der PTA GmbH
