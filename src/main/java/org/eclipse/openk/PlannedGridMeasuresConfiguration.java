/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;

public class PlannedGridMeasuresConfiguration extends Configuration {

    public static class RabbitmqConfiguration {

        @JsonProperty
        private String host;

        @JsonProperty
        private String port;

        @JsonProperty
        private String user;

        @JsonProperty
        private String password;

        @JsonProperty
        private String exchangeName;

        @JsonProperty
        private String queueNames;

        @JsonProperty
        private String unroutedMessagesExchangeName;

        @JsonProperty
        private String unroutedMessagesQueueName;


        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getExchangeName() {
            return exchangeName;
        }

        public void setExchangeName(String exchangeName) {
            this.exchangeName = exchangeName;
        }

        public String getQueueNames() { return queueNames; }

        public void setQueueNames(String queueNames) { this.queueNames = queueNames; }

        public String getUnroutedMessagesExchangeName() {
            return unroutedMessagesExchangeName;
        }

        public void setUnroutedMessagesExchangeName(String unroutedMessagesExchangeName) {
            this.unroutedMessagesExchangeName = unroutedMessagesExchangeName;
        }

        public String getUnroutedMessagesQueueName() {
            return unroutedMessagesQueueName;
        }

        public void setUnroutedMessagesQueueName(String unroutedMessagesQueueName) {
            this.unroutedMessagesQueueName = unroutedMessagesQueueName;
        }

        public List<String> getQueueNamesAsList() {

            List<String> singleQueueNameList = new ArrayList<>();
            Set<String> singleQueueNameUniqueSet = new HashSet<>();
            List<String> singleQueueNameUniqueList;

            String queueNamesString = getQueueNames();
            queueNamesString = queueNamesString.replace(";", ",");
            String[] singleQueueNames = queueNamesString.split(",");

            singleQueueNameList.addAll(Arrays.asList(singleQueueNames));
            singleQueueNameList.replaceAll(String::trim);

            singleQueueNameUniqueSet.addAll(singleQueueNameList);

            singleQueueNameUniqueList = new ArrayList<>(singleQueueNameUniqueSet);
            return singleQueueNameUniqueList;
        }
    }

    public static class EmailConfiguration {
        @JsonProperty
        private String smtpHost;

        @JsonProperty
        private String port;

        @JsonProperty
        private String sender;

        public String getSmtpHost() {
            return smtpHost;
        }

        public void setSmtpHost(String smtpHost) {
            this.smtpHost = smtpHost;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }
    }

    public static class DBConnection {
        @NotEmpty
        private String driver;

        @NotEmpty
        private String url;

        @JsonProperty
        private String user;

        @JsonProperty
        private String password;

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
    @NotEmpty
    private String persistencyUnit;

    @NotEmpty
    private String portalBaseURL;

    @NotEmpty
    private String portalFeLoginURL;

    private String whiteListDocumenttypes;

    private DBConnection dbConn;

    private EmailConfiguration emailConfiguration;

    @JsonProperty
    private RabbitmqConfiguration rabbitmqConfiguration;

    @JsonProperty
    public String getPortalBaseURL() { return this.portalBaseURL; }

    @JsonProperty
    public void setPortalBaseURL(String portalBaseURL) {this.portalBaseURL = portalBaseURL;}

    @JsonProperty
    public DBConnection getDbConn() {
        return dbConn;
    }

    @JsonProperty
    public void setDbConn(DBConnection dbConn) {
        this.dbConn = dbConn;
    }

    @JsonProperty
    public String getPersistencyUnit() { return persistencyUnit; }

    @JsonProperty
    public void setPersistencyUnit(String persistencyUnit) {this.persistencyUnit = persistencyUnit;}

    @JsonProperty
    public String getWhiteListDocumenttypes() {
        return whiteListDocumenttypes;
    }

    @JsonProperty
    public void setWhiteListDocumenttypes(String whiteListDocumenttypes) {
        this.whiteListDocumenttypes = whiteListDocumenttypes;
    }

    @JsonProperty
    public EmailConfiguration getEmailConfiguration() {
        return emailConfiguration;
    }

    @JsonProperty
    public void setEmailConfiguration(EmailConfiguration emailConfiguration) {
        this.emailConfiguration = emailConfiguration;
    }

    public String getPortalFeLoginURL() {
        return portalFeLoginURL;
    }

    public void setPortalFeLoginURL(String portalFeLoginURL) {
        this.portalFeLoginURL = portalFeLoginURL;
    }


    public RabbitmqConfiguration getRabbitmqConfiguration() {
        return rabbitmqConfiguration;
    }

    public void setRabbitmqConfiguration(RabbitmqConfiguration rabbitmqConfiguration) {
        this.rabbitmqConfiguration = rabbitmqConfiguration;
    }
}
