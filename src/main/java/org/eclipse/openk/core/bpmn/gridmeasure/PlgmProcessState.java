/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessState;

public enum PlgmProcessState implements ProcessState {
    NEW             (0),
    APPLIED         (1),
    CANCELED        (2),
    FOR_APPROVAL    (3),
    APPROVED        (4),
    REQUESTED       (5),
    RELEASED        (6),
    ACTIVE          (7),
    IN_WORK         (8),
    WORK_FINISHED   (9),
    FINISHED        (10),
    CLOSED          (11),
    REJECTED        (12),
    UNDEFINED_      (-1); // NOSONAR

    private static final Logger logger = Logger.getLogger(PlgmProcessState.class.getName());
    private final int statusValue;

    PlgmProcessState(int statusValue ) {
        this.statusValue = statusValue;
    }

    public int getStatusValue() {
        return statusValue;
    }

    public static ProcessState fromValue( int statusValue ) { // NOSONAR complexity high but simple
        switch( statusValue ) {
            case 0:
                return NEW;
            case 1:
                return APPLIED;
            case 2:
                return CANCELED;
            case 3:
                return FOR_APPROVAL;
            case 4:
                return APPROVED;
            case 5:
                return REQUESTED;
            case 6:
                return RELEASED;
            case 7:
                return ACTIVE;
            case 8:
                return IN_WORK;
            case 9:
                return WORK_FINISHED;
            case 10:
                return FINISHED;
            case 11:
                return CLOSED;
            case 12:
                return REJECTED;
            default:
                logger.error("Invalid statusValue:"+statusValue);
                return UNDEFINED_;
        }
    }
}
