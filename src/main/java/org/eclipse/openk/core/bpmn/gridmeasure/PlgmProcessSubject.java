/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure;

import org.apache.log4j.Logger;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.base.ProcessState;
import org.eclipse.openk.core.bpmn.base.ProcessSubject;

public class PlgmProcessSubject implements ProcessSubject {
    private static final Logger logger = Logger.getLogger(PlgmProcessSubject.class.getName()); // NOSONAR
    // we leave this logger in place, even if it's not called ... (at the moment)

    private GridMeasure gridMeasure;
    private String changeUser;
    private ProcessState stateInDb;
    private String jwt;

    private PlgmProcessSubject() {}

    private PlgmProcessSubject( GridMeasure gm, String changeUser ) {
        this.gridMeasure = gm;
        this.changeUser = changeUser;
    }

    public static PlgmProcessSubject fromGridMeasure( GridMeasure gm, String changeUser ) {
        return new PlgmProcessSubject(gm, changeUser);
    }

    public GridMeasure getGridMeasure() {
        return gridMeasure;
    }

    public void setGridMeasure( GridMeasure gm ) {
        this.gridMeasure = gm;
    }

    public String getChangeUser() {
        return changeUser;
    }

    public void setChangeUser(String changeUser) {
        this.changeUser = changeUser;
    }

    public ProcessState getStateInDb() {
        return stateInDb;
    }

    public void setStateInDb(ProcessState stateInDb) {
        this.stateInDb = stateInDb;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
