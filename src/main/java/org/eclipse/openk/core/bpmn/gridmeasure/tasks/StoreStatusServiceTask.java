/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.mapper.GridMeasureMapper;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.ServiceTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.util.GridMeasureStorageHelper;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.model.TblGridMeasure;

public class StoreStatusServiceTask extends ServiceTask<PlgmProcessSubject> {
    public static final Logger logger = Logger.getLogger(StoreStatusServiceTask.class.getName());
    protected final PlgmProcessState stateToSave;


    public StoreStatusServiceTask(PlgmProcessState stateToSave) {
        super("Maßnahme-Status setzen: "+stateToSave);
        this.stateToSave = stateToSave;
    }

    @Override
    protected void onLeaveStep(PlgmProcessSubject model) throws ProcessException, HttpStatusException {
        GridMeasureMapper gmMapper = new GridMeasureMapper();

        TblGridMeasure modGm = GridMeasureStorageHelper.getHelper()
                .setMeasureStatusAndStore(model.getGridMeasure().getId(),
                        model.getChangeUser(), stateToSave);

        model.setGridMeasure(gmMapper.mapToVModel(modGm));
    }
}
