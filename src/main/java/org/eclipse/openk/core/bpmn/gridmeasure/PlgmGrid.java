/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure;

import static org.eclipse.openk.api.BackendSettings.BpmnGridConfig.END_AFTER_APPROVED;
import static org.eclipse.openk.api.BackendSettings.BpmnGridConfig.END_AFTER_RELEASED;
import static org.eclipse.openk.api.BackendSettings.BpmnGridConfig.SKIP_FOR_APPROVAL;
import static org.eclipse.openk.api.BackendSettings.BpmnGridConfig.SKIP_IN_WORK;
import static org.eclipse.openk.api.BackendSettings.BpmnGridConfig.SKIP_REQUESTING;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.NO;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT1;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT2;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.PORT3;
import static org.eclipse.openk.core.bpmn.base.tasks.DecisionTask.OutputPort.YES;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.ACTIVE;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.APPLIED;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.APPROVED;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.CLOSED;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.FINISHED;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.FOR_APPROVAL;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.IN_WORK;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.NEW;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.RELEASED;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.REQUESTED;
import static org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState.WORK_FINISHED;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessGrid;
import org.eclipse.openk.core.bpmn.base.ProcessTask;
import org.eclipse.openk.core.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.core.bpmn.base.tasks.EndPointTask;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ConfigDecisionTask;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideAnotherSinglemeasure;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideMeasureApplied;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideMeasureApproved;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideMeasureNotNeededOrRequested;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideMeasurePossible;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideMeasureReleased;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.MeasureNew;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceImportData;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureAppliedMessageQ;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureApproved;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureCanceled;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureClosed;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureFinished;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureForApproval;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureRejected;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureReleased;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.ServiceMeasureRequested;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.StoreStatusServiceTask;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.UIStoreMeasureTask;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public class PlgmGrid extends ProcessGrid {
    private static Logger logger = Logger.getLogger(PlgmGrid.class);
    public PlgmGrid() {

        try {
            init();
        }
        catch( ProcessException e ) {
            logger.fatal("Invalid process grid configuration", e );
            throw new RuntimeException("Invalid process grid configuration"); // NOSONAR _fd
        }
        catch( HttpStatusException se ) {
                logger.error("Error in ProcessGrid", se );
                throw new RuntimeException("Error in ProcessGrid"); // NOSONAR _fd
        }
    }
    protected void init() throws ProcessException, HttpStatusException {

            ProcessTask measureNew = register(NEW, new MeasureNew());
            DecisionTask decideApplied = new DecideMeasureApplied();
            ProcessTask sendToMessageQOnApplied = new ServiceMeasureAppliedMessageQ(BackendConfig.getInstance().getEmailTemplatePaths().getAppliedEmailTemplate());
            ProcessTask measureApplied = register(APPLIED,
                    new UIStoreMeasureTask("State APPLIED UI Task", true));
            DecisionTask decideMeasurePossible = new DecideMeasurePossible();
            ProcessTask sendMailForApproval = new ServiceMeasureForApproval();
            DecisionTask decideSkipApproval = new ConfigDecisionTask(SKIP_FOR_APPROVAL);
            ProcessTask setMeasureToApproved = new StoreStatusServiceTask(APPROVED);
            ProcessTask sendMailOnCanceledAndMessageQ = new ServiceMeasureCanceled();
            DecisionTask decideEndAfterApproval = new ConfigDecisionTask(END_AFTER_APPROVED);
            DecisionTask decideSkipRequesting = new ConfigDecisionTask(SKIP_REQUESTING);
            ProcessTask setMeasureToActive = new StoreStatusServiceTask(ACTIVE);
            ProcessTask measureForApproval = register(FOR_APPROVAL,
                    new UIStoreMeasureTask("State FOR_APPROVAL UI Task", true));
            DecisionTask decideApproved = new DecideMeasureApproved();
            ProcessTask setMeasureToApplied = new StoreStatusServiceTask(APPLIED);
            ProcessTask sendMailApprovedAndMessageQ = new ServiceMeasureApproved();
            ProcessTask sendMailRejectedAndMessageQ = new ServiceMeasureRejected();
            ProcessTask measureApproved = register(APPROVED,
                new UIStoreMeasureTask("State APPROVED UI Task", true));
            DecisionTask decideMeasureRequested = new DecideMeasureNotNeededOrRequested();
            ProcessTask sendToMessageQueueRequested = new ServiceMeasureRequested();
            ProcessTask measureRequested = register(REQUESTED,
                new UIStoreMeasureTask("State REQUESTED UI Task", true));
            DecisionTask decideReleased = new DecideMeasureReleased();
            ProcessTask sendMailReleasedAndMessageQ = new ServiceMeasureReleased();
            ProcessTask measureReleased = register(RELEASED,
                    new UIStoreMeasureTask("State RELEASED UI Task", true));
            DecisionTask decideEndAfterReleasedTask = new ConfigDecisionTask(END_AFTER_RELEASED);
            ProcessTask setMeasureToClosed = new StoreStatusServiceTask(CLOSED);
            ProcessTask measureActive = register(ACTIVE,
                new UIStoreMeasureTask("State ACTIVE UI Task", true));
            ProcessTask measureInWork = register(IN_WORK,
                    new UIStoreMeasureTask("State IN_WORK UI Task", true));
            DecisionTask decideSkipInWorkTask = new ConfigDecisionTask(SKIP_IN_WORK);
            ProcessTask measureWorkFinished = register(WORK_FINISHED,
                    new UIStoreMeasureTask("State WORK_FINISHED UI Task", true));
            ProcessTask setMeasureToFinishedTask = new StoreStatusServiceTask(FINISHED);
            ProcessTask measureFinished = register(FINISHED,
                    new UIStoreMeasureTask("State FINISHED Task", true));
            ProcessTask importData1 = new ServiceImportData();
            ProcessTask importData2 = new ServiceImportData();
            ProcessTask importData3 = new ServiceImportData();
            ProcessTask importData4 = new ServiceImportData();
            DecisionTask decideAnotherSinglemeasure = new DecideAnotherSinglemeasure();
            ProcessTask sendMailFinishedAndMessageQ = new ServiceMeasureFinished();
            ProcessTask sendMailClosedAndMessageQ = new ServiceMeasureClosed();
            EndPointTask endPoint = new EndPointTask("PLGM Endpoint");

            // Connect
            measureNew.connectOutputTo(decideApplied);
            decideApplied.connectOutputTo(PORT1, sendToMessageQOnApplied);
            decideApplied.connectOutputTo(PORT2, measureNew);
            decideApplied.connectOutputTo(PORT3, sendMailOnCanceledAndMessageQ);
            sendMailOnCanceledAndMessageQ.connectOutputTo(endPoint);
            sendToMessageQOnApplied.connectOutputTo(measureApplied);
            measureApplied.connectOutputTo(decideMeasurePossible);
            decideMeasurePossible.connectOutputTo(YES, sendMailForApproval);
            decideMeasurePossible.connectOutputTo(NO, sendMailOnCanceledAndMessageQ);
            sendMailOnCanceledAndMessageQ.connectOutputTo(endPoint);
            sendMailForApproval.connectOutputTo(decideSkipApproval);
            decideSkipApproval.connectOutputTo(YES, setMeasureToApproved );
            decideSkipApproval.connectOutputTo(NO, measureForApproval);
            setMeasureToApproved.connectOutputTo(measureApproved);
            measureForApproval.connectOutputTo(decideApproved);
            decideApproved.connectOutputTo(PORT1, sendMailApprovedAndMessageQ);
            decideApproved.connectOutputTo(PORT2, sendMailOnCanceledAndMessageQ);
            decideApproved.connectOutputTo(PORT3, setMeasureToApplied);
            sendMailApprovedAndMessageQ.connectOutputTo(measureApproved);
            setMeasureToApplied.connectOutputTo(sendMailRejectedAndMessageQ);
            sendMailRejectedAndMessageQ.connectOutputTo(measureApplied);
            measureApproved.connectOutputTo(decideEndAfterApproval);
            decideEndAfterApproval.connectOutputTo(NO, decideSkipRequesting);
            decideEndAfterApproval.connectOutputTo(YES, setMeasureToClosed);
            decideSkipRequesting.connectOutputTo(NO, decideMeasureRequested);
            decideSkipRequesting.connectOutputTo(YES, setMeasureToActive);
            decideMeasureRequested.connectOutputTo(YES, sendMailOnCanceledAndMessageQ);
            decideMeasureRequested.connectOutputTo(NO, sendToMessageQueueRequested);
            sendToMessageQueueRequested.connectOutputTo(measureRequested);
            measureRequested.connectOutputTo(decideReleased);
            decideReleased.connectOutputTo(PORT1, sendMailReleasedAndMessageQ);
            decideReleased.connectOutputTo(PORT2, setMeasureToApplied);
            decideReleased.connectOutputTo(DecisionTask.OutputPort.PORT3, sendMailOnCanceledAndMessageQ);
            sendMailReleasedAndMessageQ.connectOutputTo(measureReleased);
            setMeasureToApplied.connectOutputTo(sendMailRejectedAndMessageQ);
            sendMailRejectedAndMessageQ.connectOutputTo(measureApplied);
            setMeasureToActive.connectOutputTo(decideEndAfterReleasedTask);
            measureReleased.connectOutputTo(decideEndAfterReleasedTask);
            decideEndAfterReleasedTask.connectOutputTo(NO, importData1);
            decideEndAfterReleasedTask.connectOutputTo(YES, setMeasureToClosed);
            setMeasureToClosed.connectOutputTo(sendMailClosedAndMessageQ);
            importData1.connectOutputTo(measureActive);
            measureActive.connectOutputTo(decideSkipInWorkTask);
            decideSkipInWorkTask.connectOutputTo(NO, importData2);
            decideSkipInWorkTask.connectOutputTo(YES, setMeasureToFinishedTask);
            importData2.connectOutputTo(measureInWork);
            measureInWork.connectOutputTo(importData3);
            importData3.connectOutputTo(measureWorkFinished);
            setMeasureToFinishedTask.connectOutputTo(sendMailFinishedAndMessageQ);
            measureWorkFinished.connectOutputTo(importData4);
            importData4.connectOutputTo(decideAnotherSinglemeasure);
            decideAnotherSinglemeasure.connectOutputTo(YES, measureApproved);
            decideAnotherSinglemeasure.connectOutputTo(NO, sendMailFinishedAndMessageQ);
            sendMailFinishedAndMessageQ.connectOutputTo(measureFinished);
            measureFinished.connectOutputTo(sendMailClosedAndMessageQ);
            sendMailClosedAndMessageQ.connectOutputTo(endPoint);
    }

}
