/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;

public abstract class ProcessGrid {
    public static class Recoverable<T extends ProcessSubject> {
        private final T subject;
        private final ProcessGrid instance;

        public Recoverable( T subject, ProcessGrid instance ) {
            this.subject = subject;
            this.instance = instance;
        }
        public void start(StateResolver resolver ) throws ProcessException, HttpStatusException {
            ProcessTask t = instance.resolve( resolver.resolveState());
            t.recover(subject);
        }
    }

    private static final Logger logger= Logger.getLogger(ProcessGrid.class.getName());
    private Map<ProcessState, ProcessTask> gridMap = new HashMap<>();

    public <T extends ProcessSubject> Recoverable<T> recover(T subject) {
        return new Recoverable<>( subject, this);
    }

    protected <P extends ProcessTask> P register(ProcessState state, P step ) {
        gridMap.put( state, step );
        return step;
    }


    protected ProcessTask resolve(ProcessState state ) throws ProcessException {
        if( !gridMap.containsKey(state) ) {
            logger.error("State" + state.toString() + " is not resolvable!");
            throw new ProcessException("State " + state.toString() + " is not resolvable!");
        }
        else {
            return gridMap.get(state);
        }
    }
}
