/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessSubject;
import org.eclipse.openk.core.bpmn.base.ProcessTask;


public class EndPointTask implements ProcessTask {
    private static final Logger logger= Logger.getLogger(EndPointTask.class.getName());
    private final String description;

    public EndPointTask(String description ) {
        this.description = description;
    }
    @Override
    public void enterStep(ProcessSubject model) throws ProcessException {
        logger.debug("Endpoint: \""+description+"\"");
    }

    @Override
    public void leaveStep(ProcessSubject model) throws ProcessException {
        throw new ProcessException("Cannot leave Endpoint: " + description);
    }

    @Override
    public void connectOutputTo(ProcessTask step) throws ProcessException {
        throw new ProcessException("Cannot connect to an Endpoint: "+description);
    }

    @Override
    public void recover(ProcessSubject model) throws ProcessException {
        throw new ProcessException("Cannot recover at an Endpoint: "+description);
    }
}
