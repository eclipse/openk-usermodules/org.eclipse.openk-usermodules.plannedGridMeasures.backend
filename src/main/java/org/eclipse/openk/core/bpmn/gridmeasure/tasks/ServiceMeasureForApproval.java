/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.ServiceTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.messagebroker.Producer;

public class ServiceMeasureForApproval extends ServiceTask<PlgmProcessSubject> {
    private static final Logger logger= Logger.getLogger(ServiceMeasureForApproval.class.getName());
    public ServiceMeasureForApproval() {
        super("Service task 'zur Genehmigung'");
    }


    @Override
    protected void onLeaveStep(PlgmProcessSubject model) throws ProcessException {

        try (Producer prod = createMessageQueueProducer()) {
            prod.sendMessageAsJson(model.getGridMeasure(), "forapproval");
        } catch (Exception e) {
            logger.error("Error in ServiceMeasureForApproval onLeaveStep (RabbitMQ)", e);
            throw new ProcessException("Error in ServiceMeasureForApproval onLeaveStep (RabbitMQ)");
        }
        logger.debug(">>execute: basicPublish RabbitMQ");
    }
}
