/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.apache.log4j.Logger;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.ServiceTask;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.EmailManager;
import org.eclipse.openk.core.controller.PgmEmail;

import javax.mail.MessagingException;


public class ServiceMeasureApplied extends ServiceTask<PlgmProcessSubject> {
    private static final Logger logger= Logger.getLogger(ServiceMeasureApplied.class.getName());

    public ServiceMeasureApplied() {
        super("Service task 'beantragt'");
    }

    @Override
    protected void onLeaveStep(PlgmProcessSubject model) throws ProcessException {

        try {
            EmailTemplatePaths paths = BackendConfig.getInstance().getEmailTemplatePaths();
            String template = paths.getAppliedEmailTemplate();
            EmailManager emailManager = new PgmEmail(model, template, false);
            emailManager.sendEmail();
        } catch (MessagingException e) {
            logger.error("Error in ServiceMeasureApplied onLeaveStep", e);
            throw new ProcessException("Error in ServiceMeasureApplied onLeaveStep (EmailManager)");
        }

        logger.debug(">>execute: Send Mail on 'beantragt'");
    }


}
