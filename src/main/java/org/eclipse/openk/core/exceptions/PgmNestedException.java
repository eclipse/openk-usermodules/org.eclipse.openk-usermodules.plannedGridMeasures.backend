/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.exceptions;

import org.eclipse.openk.core.viewmodel.ErrorReturn;

public class PgmNestedException extends HttpStatusException {
    private static final long serialVersionUID = 6771249960550163561L;
    private final ErrorReturn errorReturn;

    public PgmNestedException(ErrorReturn er) {
        super(er.getErrorCode());
        errorReturn = er;
    }

    public PgmNestedException(ErrorReturn er, Throwable t) {
        super(er.getErrorCode(), "Error message",  t);
        errorReturn = er;
    }

    public ErrorReturn getErrorReturn() {
        return errorReturn;
    }

    @Override
    public int getHttpStatus() {
        return errorReturn.getErrorCode();
    }
}
