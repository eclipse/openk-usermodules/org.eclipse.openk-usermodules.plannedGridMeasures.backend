/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.exceptions;

import org.apache.http.HttpStatus;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.eclipse.openk.core.viewmodel.GeneralReturnItem;

public final class PgmExceptionMapper {
    private PgmExceptionMapper() {}

    public static String unknownErrorToJson() {
        ErrorReturn er = new ErrorReturn();
        er.setErrorText("Unknown Error");
        er.setErrorCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        return JsonGeneratorBase.getGson().toJson(er);
    }

    public static String toJson(HttpStatusException e) {
            ErrorReturn er = new ErrorReturn();
            er.setErrorText(e.getMessage());
        er.setErrorCode(e.getHttpStatus());
            return JsonGeneratorBase.getGson().toJson(er);
    }

    public static String getGeneralErrorJson() {
        return JsonGeneratorBase.getGson().toJson(new GeneralReturnItem("NOK"));
    }

    public static String getGeneralOKJson() {
        return JsonGeneratorBase.getGson().toJson(new GeneralReturnItem("OK"));
    }
}
