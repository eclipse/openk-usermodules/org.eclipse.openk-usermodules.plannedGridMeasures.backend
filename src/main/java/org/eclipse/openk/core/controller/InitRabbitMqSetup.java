/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.core.controller;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import org.apache.log4j.Logger;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.RabbitmqConfiguration;

public class InitRabbitMqSetup {

    private static final Logger LOGGER = Logger.getLogger(GridMeasureBackendController.class);

    public void init()  {
        LOGGER.debug("InitRabbitMqSetup started");
        RabbitmqConfiguration rabbitmqConfiguration = BackendConfig.getInstance().getRabbitmqConfiguration();
        if (rabbitmqConfiguration == null) {
            LOGGER.warn("InitRabbitMqSetup: No RabbitmqConfiguration found!");
            return;
        }

        ConnectionFactory factory = createRabbitFactory(rabbitmqConfiguration);
        try (Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()) {

            String unroutedMessagesExchangeName = BackendConfig.getInstance().getRabbitmqConfiguration().getUnroutedMessagesExchangeName();
            Map<String, Object> exchangeArgs = new HashMap<String, Object>();
            exchangeArgs.put("alternate-exchange", unroutedMessagesExchangeName);
            channel
                .exchangeDeclare(BackendConfig.getInstance().getRabbitmqConfiguration().getExchangeName(),
                    BuiltinExchangeType.DIRECT, true, false, exchangeArgs);

            String unroutedMessagesQueueName = BackendConfig.getInstance().getRabbitmqConfiguration().getUnroutedMessagesQueueName();

            // durable = true
            channel.exchangeDeclare(unroutedMessagesExchangeName, BuiltinExchangeType.FANOUT, true);
            channel.queueDeclare(unroutedMessagesQueueName, true, false, false, null);
            channel.queueBind(unroutedMessagesQueueName, unroutedMessagesExchangeName, "");

            Map<String, Object> args = new HashMap<>();
            args.put("x-queue-mode", "lazy");

            List<String> queueNames = rabbitmqConfiguration.getQueueNamesAsList();

            for (String queueName : queueNames) {
                channel.queueDeclare(queueName, true, false, false, args);
                String routingKey = queueName.split("-")[1];
                channel.queueBind(queueName, rabbitmqConfiguration.getExchangeName(), routingKey);
            }

            LOGGER.debug("InitRabbitMqSetup finished");
        } catch (TimeoutException | IOException e) {
            LOGGER.error("Error in InitRabbitMqSetup", e);
        }
    }




    private static ConnectionFactory createRabbitFactory(RabbitmqConfiguration rabbitmqConfiguration) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitmqConfiguration.getHost());
        factory.setPassword(rabbitmqConfiguration.getPassword());
        factory.setUsername(rabbitmqConfiguration.getUser());
        factory.setPort(Integer.parseInt(rabbitmqConfiguration.getPort()));
        return factory;
    }


}
