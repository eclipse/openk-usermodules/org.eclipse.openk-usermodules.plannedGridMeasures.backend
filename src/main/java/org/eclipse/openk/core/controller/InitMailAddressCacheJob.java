/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.core.controller;

import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.Logger;

public class InitMailAddressCacheJob {

    private static final Logger LOGGER = Logger.getLogger(GridMeasureBackendController.class);

    public void init()  {
        LOGGER.debug("InitMailAddressCollectionJob called");
        TimerTask timerTask = new MailAddressCacheTimerTask();
        Timer timer = new Timer();

        long reloadInterval = BackendConfig.getInstance().getBackendSettings().getReloadMailAddressesInMin()*60L * 1000L;

        timer.scheduleAtFixedRate(timerTask, 500, reloadInterval);
    }







}
