/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.core.controller;

import org.apache.log4j.Logger;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.EmailConfiguration;
import org.eclipse.openk.api.mail.Email;
import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;

import javax.mail.MessagingException;

public abstract class EmailManager {

  protected static final Logger LOGGER = Logger.getLogger(EmailManager.class.getName());

  protected Email email;
  private String sender;

  protected String templatePath;
  protected boolean loadRecipientsFromGm;

  private String recipientsTo;
  private String recipientsCC;
  protected String subject;
  protected String emailBody;

  protected PlgmProcessSubject processSubject;
  private final EmailConfiguration emailConfiguration;

  public EmailManager(PlgmProcessSubject model, String templatePath, boolean loadRecipientsFromGm) throws MessagingException {
    this.templatePath = templatePath;
    this.loadRecipientsFromGm = loadRecipientsFromGm;
    emailConfiguration = BackendConfig.getInstance().getEmailConfiguration();
    this.sender = BackendConfig.getInstance().getEmailConfiguration().getSender();
    this.processSubject = model;
    email = new Email(emailConfiguration.getSmtpHost(), emailConfiguration.getPort());
    email.setFrom(sender);
  }

  protected void init() throws MessagingException {
    this.loadEmailTemplate();
    this.setRecipients();
    this.prepareEmailContent();
  }

  private void loadEmailTemplate() {
    LOGGER.debug("start loading EmailTemplate");
    ResourceLoaderBase loaderBase = new ResourceLoaderBase();
    String templateString = loaderBase.loadFromPath(templatePath);
    String[] templateSplit = templateString.split("Body:");
    String emailHead = templateSplit[0];
    this.emailBody = templateSplit[1];
    String[] emailHeadSplit = emailHead.split("Subject:");
    String recipients = emailHeadSplit[0];
    String[] recipientsSplit = recipients.split("CC:");
    recipientsTo = recipientsSplit[0].trim().replace("To:", "");
    recipientsCC = recipientsSplit[1].trim();
    this.subject = emailHeadSplit[1].trim();
    LOGGER.debug("finsished loading EmailTemplate");
  }

  protected abstract void prepareEmailContent();

  private void setRecipients() throws MessagingException {
    String recipientsFromGm;

    recipientsFromGm = this.processSubject.getGridMeasure().getEmailAddresses();

    if(this.loadRecipientsFromGm && recipientsFromGm != null) {
        recipientsFromGm = "," + recipientsFromGm;
        email.addRecipient(recipientsTo + recipientsFromGm);
    }
    else{
      email.addRecipient(recipientsTo);
    }
    email.addCC(recipientsCC);
  }

  private void prepareMailToSend() throws MessagingException {
    email.addText(emailBody);
    email.setSubject(subject);
    email.setContent();
  }

  public boolean sendEmail() throws MessagingException {

    try {
      prepareMailToSend();
      boolean ret = email.send();
      if (ret) {
        LOGGER.info("Email with subject: " + email.getMessage().getSubject() + " has been sent successfully");
      } else {
        LOGGER.error("Some error occured while sending email with subject: " + email.getMessage().getSubject() + " . Email wasn't sent.");
        throw new MessagingException();
      }
      return true;
    } catch (MessagingException e) {
      LOGGER.error("Error occured in sendEmail", e);
      throw e;
    }
  }

}
