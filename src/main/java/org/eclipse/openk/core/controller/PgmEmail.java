/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.core.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.communication.RestServiceWrapper;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import javax.mail.MessagingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class PgmEmail extends EmailManager {

  private static final String SUBJECT_DATE_PATTERN_APPLIED = "EEEE', den' dd.MM.yy 'um' HH:mm:ss";
  protected Map<String,String> contentReplaceMap;
  private static String portalFeUrl = null;
  private static final Object portalLoadingLock = new Object();
  private String jwt = null;

  public PgmEmail(PlgmProcessSubject model, String templatePath, boolean loadRecipientsFromGm) throws MessagingException {
    super(model, templatePath, loadRecipientsFromGm);
    this.jwt = model.getJwt();
    init();
  }

  @Override
  public void prepareEmailContent() {
    LOGGER.debug("Modifying Email...");
    createContentReplacHashMap();

    for (Entry<String, String> stringStringEntry : contentReplaceMap.entrySet()) {
      emailBody = emailBody.replace(stringStringEntry.getKey(),stringStringEntry.getValue());
      subject = subject.replace(stringStringEntry.getKey(),stringStringEntry.getValue());
    }

    LOGGER.debug("...modifying Email finished");
  }

  protected void createContentReplacHashMap(){
    GridMeasure currentGridMeasure = processSubject.getGridMeasure();
    this.contentReplaceMap = new HashMap<>();
    contentReplaceMap.put("$gridMeasureTitle$", currentGridMeasure.getTitle());

    String directMeasureLink = getDirectMeasureLink(currentGridMeasure.getId());
    contentReplaceMap.put("$directMeasureLink$", directMeasureLink);

    Date plannedStarttimeFirstSinglemeasure = currentGridMeasure.getPlannedStarttimeFirstSinglemeasure();
    if(plannedStarttimeFirstSinglemeasure != null) {
      DateFormat dfmt = new SimpleDateFormat(SUBJECT_DATE_PATTERN_APPLIED);
      String formattedDate = dfmt.format(plannedStarttimeFirstSinglemeasure);
      contentReplaceMap.put("$plannedStarttimeFirstSinglemeasure$", formattedDate);
    }

    Date endtimeGridmeasure = currentGridMeasure.getEndtimeGridmeasure();
    if(endtimeGridmeasure != null) {
      DateFormat dfmt = new SimpleDateFormat(SUBJECT_DATE_PATTERN_APPLIED);
      String formattedDate = dfmt.format(endtimeGridmeasure);
      contentReplaceMap.put("$endtimeGridmeasure$", formattedDate);
    }

    String remark = currentGridMeasure.getRemark();
    if(remark != null) {
      contentReplaceMap.put("$gridMeasureRemark$", remark);
    }

  }

  private String getDirectMeasureLink(Integer measureId) {
    // Example Link: http://localhost:4220/#/gridMeasureDetail/5/edit?fwdUrl=http%3A%2F%2Flocalhost%3A4201%2F%23%2Flogin
    // %3FfwdUrl%3Dhttp%3A%2F%2Flocalhost%3A4220%2F%26fwdId%3D5
    String portalFeLoginUrl = BackendConfig.getInstance().getPortalFeLoginUrl();
    initPortalFeUrl(this.jwt);

    String stdGridmeasureURL = portalFeUrl + "#/" + "gridMeasureDetail/" + measureId + "/edit";

    String fwdURL = "fwdUrl=" + portalFeUrl + "&fwdId=" + measureId;
    String fwdURLEncoded = "";
    String finalFwdUrl = portalFeLoginUrl + "?" + fwdURL;
    try {
      fwdURLEncoded = URLEncoder.encode(finalFwdUrl, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      LOGGER.error("Error in getDirectMeasureLink", e);
      return "";
    }

    return stdGridmeasureURL + "?fwdUrl=" + fwdURLEncoded;
  }

  private static void initPortalFeUrl(String jwt)  {
    if( jwt == null ) {
      LOGGER.info("Empty JWT passed to EMailManger");
      portalFeUrl = "";
    }
    synchronized (portalLoadingLock) {
      if( portalFeUrl == null ) {
        RestServiceWrapper wrapper = createWrapper();
        try {
          portalFeUrl =  extractPortalFE(wrapper.performGetRequest("userModulesForUser", jwt));
        } catch (HttpStatusException e) {
          LOGGER.warn("Modules for User could not be resolved", e);
        }
      }
    }
  }

  private static RestServiceWrapper createWrapper() {
    return new RestServiceWrapper(BackendConfig.getInstance().getPortalBaseUrl(), false);
  }

  private static String extractPortalFE( String moduleConfig ) {
    String retUrl = "<PortalFE-URL>";
    JsonParser parser = new JsonParser();
    JsonElement je = parser.parse(moduleConfig);
    JsonArray moduleArray = je.getAsJsonArray();
    for( JsonElement moduleElem : moduleArray ) {
      String moduleNameUpper = moduleElem.getAsJsonObject().get("name").getAsString().toUpperCase();
      if( moduleNameUpper.contains("PLAN") && moduleNameUpper.contains("NAHME") ) {
        return moduleElem.getAsJsonObject().get("link").getAsString();
      }
    }
    return retUrl;
  }


}
