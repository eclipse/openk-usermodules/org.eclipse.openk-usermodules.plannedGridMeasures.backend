/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;


import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.auth2.model.JwtPayload;
import org.eclipse.openk.auth2.model.JwtRealmAccess;
import org.eclipse.openk.auth2.util.JwtHelper;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.communication.RestServiceWrapper;
import org.eclipse.openk.core.controller.BaseWebService.SecureType;
import org.eclipse.openk.core.exceptions.HttpStatusException;

import java.util.List;

public class TokenManager {
    private static final Logger LOGGER = Logger.getLogger(TokenManager.class.getName());

    private static final TokenManager INSTANCE = new TokenManager();

    private TokenManager() {
    }

    public static TokenManager getInstance() {
        return INSTANCE;
    }

    public void logout(String token) throws HttpStatusException {
        RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getPortalBaseUrl(), false);
        restServiceWrapper.performGetRequest("logout",token);
    }

    public void checkAut(String token) throws HttpStatusException {
        RestServiceWrapper restServiceWrapper = new RestServiceWrapper(BackendConfig.getInstance().getPortalBaseUrl(), false);
        restServiceWrapper.performGetRequest("checkAuth",token);
    }

    public void checkAutLevel(String token, SecureType secureType) throws HttpStatusException {
        JwtPayload jwtPayload = JwtHelper.getJwtPayload(token);
        boolean isNormalUser = jwtPayload.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_NORMALUSER);
        boolean isSuperUser = jwtPayload.getRealmAccess().isInRole(Globals.KEYCLOAK_ROLE_SUPERUSER);

        boolean notAuth = secureType == SecureType.NORMAL && !(isNormalUser || isSuperUser);
        notAuth |= (secureType == SecureType.HIGH && !isSuperUser);

        if ( notAuth )
        {
            LOGGER.warn("Security level not sufficent ");
            throw new HttpStatusException(HttpStatus.SC_FORBIDDEN);
        }
    }

    public List<String> getUserRoles(String token) throws HttpStatusException {
        JwtPayload jwtPayload = JwtHelper.getJwtPayload(token);
        JwtRealmAccess jra = jwtPayload.getRealmAccess();
        return jra.getRoles();
    }

}
