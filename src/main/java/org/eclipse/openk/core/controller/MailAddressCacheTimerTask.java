/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.core.controller;

import java.util.*;

import org.apache.log4j.Logger;
import org.eclipse.openk.db.dao.AutoCloseEntityManager;
import org.eclipse.openk.db.dao.EntityHelper;
import org.eclipse.openk.db.dao.TblGridMeasureDao;

import javax.persistence.EntityManager;


public class MailAddressCacheTimerTask extends TimerTask {

    private static final Logger LOGGER = Logger.getLogger(MailAddressCacheTimerTask.class.getName());

    @Override
    public void run() {
        LOGGER.debug("MailAddressCacheTimerTask called");
        try {

            List<String> mailAddressStrings = new ArrayList<>();
            List<String> singleMailAddressList = new ArrayList<>();
            Set<String> mailAddressesUniqueSet = new HashSet<>();
            List<String> mailAddressesUniqueList = new ArrayList<>();

            try (AutoCloseEntityManager em = createEm()) {
                TblGridMeasureDao gmDao = createTblGridMeasureDao(em);
                mailAddressStrings = gmDao.getMailAddressesFromGridmeasures();
            }

            while (mailAddressStrings.remove(null));

            for (String mailAddressString : mailAddressStrings) {

                mailAddressString = mailAddressString.replace(";", ",");
                String[] singleMailAddresses = mailAddressString.split(",");
                singleMailAddressList.addAll(Arrays.asList(singleMailAddresses));
                singleMailAddressList.replaceAll(String::trim);

                mailAddressesUniqueSet.addAll(singleMailAddressList);
            }

            mailAddressesUniqueList = new ArrayList<>(mailAddressesUniqueSet);
            MailAddressCache.getInstance().setMailAddresses(mailAddressesUniqueList);

        }
        catch (Exception e) {
            LOGGER.error("Error in MailAddressCacheTimerTask",e);
        }
    }


    protected AutoCloseEntityManager createEm() {
        return new AutoCloseEntityManager(EntityHelper.getEMF().createEntityManager());
    }

    protected TblGridMeasureDao createTblGridMeasureDao(EntityManager em) { return new TblGridMeasureDao(em); }


}
