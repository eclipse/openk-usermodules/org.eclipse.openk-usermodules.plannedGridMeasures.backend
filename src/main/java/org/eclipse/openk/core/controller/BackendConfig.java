/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import static org.eclipse.openk.common.JsonGeneratorBase.getGson;

import edu.emory.mathcs.backport.java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.EmailConfiguration;
import org.eclipse.openk.PlannedGridMeasuresConfiguration.RabbitmqConfiguration;
import org.eclipse.openk.api.BackendSettings;
import org.eclipse.openk.api.mail.EmailTemplatePaths;
import org.eclipse.openk.common.util.ResourceLoaderBase;


public class BackendConfig {
    private static final Logger logger = Logger.getLogger(BackendConfig.class);
    private String portalBaseUrl;
    private String portalFeLoginUrl;
    private List<String> whiteListDocumenttypes = new ArrayList<>();
    private EmailConfiguration emailConfiguration;
    private RabbitmqConfiguration rabbitmqConfiguration;

    private EmailTemplatePaths emailTemplatePaths;
    private BackendSettings backendSettings;

    private static final Object LOCK = new Object();

    private static BackendConfig instance;

    private BackendConfig() {}

    public static void configure(PlannedGridMeasuresConfiguration configurationFromYaml, BackendSettings backendSettings) {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new BackendConfig();
                instance.init();
                instance.portalBaseUrl = configurationFromYaml.getPortalBaseURL();
                instance.portalFeLoginUrl = configurationFromYaml.getPortalFeLoginURL();
                instance.backendSettings = backendSettings;
                instance.rabbitmqConfiguration = configurationFromYaml.getRabbitmqConfiguration();
                return;
            }
            logger.warn("BackendConfig initialized more than once!");
        }
    }

    public static BackendConfig getInstance() {
        synchronized (LOCK) {
            if( instance == null ) {
                String errormsg = "BackendConfig accessed without being initialized!";
                logger.error(errormsg);
                throw new RuntimeException(errormsg); // NOSONAR
            }
            return instance;
        }
    }

    private void init(){
        initEmailTemplatePaths();
    }


    private void initEmailTemplatePaths(){
        ResourceLoaderBase loaderBase = new ResourceLoaderBase();
        String fromPath = loaderBase.loadFromPath("emailConfiguration/mailTemplatesPaths.json");
        EmailTemplatePaths templatePaths = getGson().fromJson(fromPath, EmailTemplatePaths.class);
        setEmailTemplatePaths(templatePaths);
    }

    public String getPortalBaseUrl() {
        return portalBaseUrl;
    }

    public void setPortalBaseUrl(String portabBaseUrl) {
        this.portalBaseUrl = portabBaseUrl;
    }

    public List<String> getWhiteListDocumenttypes() {
        return whiteListDocumenttypes;
    }

    public void setWhiteListDocumenttypes(List<String> whiteListDocumenttypes) {
        this.whiteListDocumenttypes = whiteListDocumenttypes;
    }

    public void processAndSetWhiteListDocumenttypes(String commaSeperatedList){
        List<String> whiteList = new ArrayList<>();
        if (commaSeperatedList != null) {
            String[] split = commaSeperatedList.split(",");
            whiteList = new ArrayList<>(Arrays.asList(split));
        }

        setWhiteListDocumenttypes(whiteList);
    }

    public EmailConfiguration getEmailConfiguration() {
        return emailConfiguration;
    }

    public void setEmailConfiguration(EmailConfiguration emailConfiguration) {
        this.emailConfiguration = emailConfiguration;
    }

    public EmailTemplatePaths getEmailTemplatePaths() {
        return emailTemplatePaths;
    }

    public void setEmailTemplatePaths(EmailTemplatePaths emailTemplatePaths) {
        this.emailTemplatePaths = emailTemplatePaths;
    }

    public BackendSettings getBackendSettings() {
        return backendSettings;
    }

    public String getPortalFeLoginUrl() {
        return portalFeLoginUrl;
    }

    public void setPortalFeLoginUrl(String portalFeLoginUrl) {
        this.portalFeLoginUrl = portalFeLoginUrl;
    }

    public static void resetBackendConfigUnitTests(){
        instance = null;
    }

    public RabbitmqConfiguration getRabbitmqConfiguration() {
        return rabbitmqConfiguration;
    }

    public void setRabbitmqConfiguration(RabbitmqConfiguration rabbitmqConfiguration) {
        this.rabbitmqConfiguration = rabbitmqConfiguration;
    }
}
