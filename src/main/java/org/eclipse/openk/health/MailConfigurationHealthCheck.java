/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.health;

import com.codahale.metrics.health.HealthCheck;
import org.eclipse.openk.api.mail.Email;
import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.controller.BackendConfig;

public class MailConfigurationHealthCheck extends HealthCheck {

    private String getEmailTemplatePath()
    {
        return BackendConfig.getInstance().getEmailTemplatePaths().getAppliedEmailTemplate();
    }

    private String getEmailTemplateString(String emailTemplatePath)
    {
        ResourceLoaderBase loaderBase = new ResourceLoaderBase();
        return loaderBase.loadFromPath(emailTemplatePath);
    }

        @Override
        protected Result check() throws Exception {

            String templatePath = getEmailTemplatePath();
            String templateString = getEmailTemplateString(templatePath);

            String[] templateSplit = templateString.split("Body:");
            String emailHead = templateSplit[0]; //Header
            String[] emailHeadSplit = emailHead.split("Subject:");
            String recipients = emailHeadSplit[0]; //all Recipients
            String[] recipientsSplit = recipients.split("CC:");
            String recipientsTo = recipientsSplit[0].trim().replace("To:", "");
            String recipientsCC = "";
            if (recipientsSplit.length > 1) {
                recipientsCC = recipientsSplit[1].trim();
            }

            if (!recipientsTo.isEmpty() && !recipientsCC.isEmpty()) {

                String[] recipientToList = recipientsTo.split(",");
                String[] recipientCCList = recipientsCC.split(",");

                for (String recipientToEmail: recipientToList) {
                    if (!Email.validateEmailAddress(recipientToEmail.trim())) {
                        return Result.unhealthy("recipients To email-address is not valid");
                    }
                }
                for (String recipientCCEmail: recipientCCList) {
                    if (!Email.validateEmailAddress(recipientCCEmail.trim())) {
                        return Result.unhealthy("recipients CC email-address is not valid");
                    }
                }
                return Result.healthy();

            }
            else {
                return Result.unhealthy("Applied Email-Template not found or wrong Email-Configuration");
            }
        }
    }





