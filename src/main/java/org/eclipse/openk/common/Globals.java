/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.common;

public final class Globals {

    public static final String HEADER_JSON_UTF8 = "application/json; charset=utf-8";
    public static final String KEYCLOAK_AUTH_TAG = "Authorization";
    public static final String KEYCLOAK_ROLE_SUPERUSER = "planned-policies-superuser";
    public static final String KEYCLOAK_ROLE_NORMALUSER = "planned-policies-normaluser";
    public static final String SESSION_TOKEN_TAG = "X-XSRF-TOKEN";
    public static final String FORCE_DELETE_LOCK = "FORCE";

    public static final String COUNTERTYPE_GRIDMEASURE_ID = "GridMeasureId";

    public static final String DATE_ZONE_ID_EUROPE = "Europe/Berlin";

    private Globals() {}
}
