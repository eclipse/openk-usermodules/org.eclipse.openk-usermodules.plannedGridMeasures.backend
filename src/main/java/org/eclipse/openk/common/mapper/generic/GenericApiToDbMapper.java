/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.common.mapper.generic;

import com.google.common.collect.Lists;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelClass;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelField;

public class GenericApiToDbMapper {

    public <T> T mapToViewModel( Class<T> viewModelClass,  Object dbpojo ) {

        try {
            Class cdbm = null;
            cdbm = Class.forName(dbpojo.getClass().getName());

            assertVMClassAnnotation(viewModelClass, cdbm);

            Field[] vmClassFields = viewModelClass.getDeclaredFields();

            Map<String, Field> db2vmFieldMap = new HashMap<>();
            fillFieldMap(vmClassFields, db2vmFieldMap);

            return createAndMapObject(viewModelClass, dbpojo, cdbm, db2vmFieldMap);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Class not found in mapToViewModel", e); // NOSONAR RuntimeException by design here!
        }
    }


    public <T> T mapFromViewModel( Class<T> dbModelClass, Object vmpojo ) {
        try {
            Class cvm = null;

            cvm = Class.forName(vmpojo.getClass().getName());

            assertVMClassAnnotation(cvm, dbModelClass);

            Field[] dbClassFields = dbModelClass.getDeclaredFields();
            Field[] vmClassFields = cvm.getDeclaredFields();

            Map<String, Field> vm2dbFieldMap = new HashMap<>();
            fillReverseFieldMap(vmClassFields, dbClassFields, vm2dbFieldMap);

            return createAndMapObject(dbModelClass, vmpojo, cvm, vm2dbFieldMap);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Class not found in mapFromViewModel", e); // NOSONAR RuntimeException by design here!
        }

    }

    private void assertVMClassAnnotation( Class vmClazz, Class dbmClazz ) { // NOSONAR Generic Exception tolerated here _fd
        MapDbModelClass clazzAnno = (MapDbModelClass)vmClazz.getAnnotation(MapDbModelClass.class); // NOSONAR wrong sonar message
        if( clazzAnno == null || !clazzAnno.classType().equals(dbmClazz) ) {
            throw new RuntimeException("View Model class \""+vmClazz.getSimpleName()+"\" is missing annotation: @" + MapDbModelClass.class.getSimpleName() + // NOSONAR Generic Exception tolerated here _fd
                    "(classType = " + dbmClazz.getSimpleName() +".class)");
        }

    }


    private <T> T createAndMapObject(Class<T> trgClass, Object srcObj, Class srcClazz, Map<String, Field> db2vmFieldMap) {
        T vmTarget = createTargetObject(trgClass);

        for( Field dbfield : srcClazz.getDeclaredFields()) {
            if( db2vmFieldMap.containsKey(dbfield.getName())) {
                Field targetfield = db2vmFieldMap.get(dbfield.getName());

                storeTargetValue(srcObj, dbfield, vmTarget, targetfield);
            }
        }

        return vmTarget;
    }

    private void storeTargetValue( Object srcObj, Field srcField, Object trgObj, Field trgField) {// NOSONAR high oyclic complexity tolerated here
        try {
            if (trgField.getType().equals(srcField.getType())) {
                srcField.setAccessible(true);
                trgField.setAccessible(true);

                if (trgField.getType().equals(int.class)) {
                    trgField.setInt(trgObj, srcField.getInt(srcObj));
                    return;
                }

                if (trgField.getType().equals(boolean.class)) {
                    trgField.setBoolean(trgObj, srcField.getBoolean(srcObj));
                    return;
                }

                if (trgField.getType().equals(byte.class)) {
                    trgField.setByte(trgObj, srcField.getByte(srcObj));
                    return;
                }

                if (trgField.getType().equals(char.class)) {
                    trgField.setChar(trgObj, srcField.getChar(srcObj));
                    return;
                }

                if (trgField.getType().equals(double.class)) {
                    trgField.setDouble(trgObj, srcField.getDouble(srcObj));
                    return;
                }

                if (trgField.getType().equals(float.class)) {
                    trgField.setFloat(trgObj, srcField.getFloat(srcObj));
                    return;
                }

                if (trgField.getType().equals(long.class)) {
                    trgField.setLong(trgObj, srcField.getLong(srcObj));
                    return;
                }

                if (trgField.getType().equals(short.class)) {
                    trgField.setShort(trgObj, srcField.getShort(srcObj));
                    return;
                }

                if (trgField.getType() instanceof Object) {
                    trgField.set(trgObj, srcField.get(srcObj));
                    return; // NOSONAR we do not remove this return statement
                }
            }
        }
        catch( IllegalAccessException i ) {
            throw new RuntimeException("Type error while mapping.", i); // NOSONAR RuntimeException by design here!
        }

    }

    protected <T> T createTargetObject(Class<T> viewModelClass) {  // NOSONAR generic Exception tolerated
        T vmTarget;
        try {
            vmTarget = viewModelClass.getConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Target pojo-class needs a default constructor!", e); // NOSONAR generic Exception tolerated
        }
        return vmTarget;
    }

    protected void fillFieldMap(Field[] vmClassFields, Map<String, Field> fieldMap) {
        for( Field f : vmClassFields ) {
            MapDbModelField fieldAnnotation = f.getAnnotation(MapDbModelField.class);
            if( fieldAnnotation != null ) {
                if( fieldAnnotation.fieldName().isEmpty() ) {
                    fieldMap.put(f.getName(), f);
                }
                else
                {
                    fieldMap.put(fieldAnnotation.fieldName(), f);
                }
            }
        }
    }

    protected void fillReverseFieldMap(Field[] vmClassFields, Field[] dbClassFields, Map<String, Field> fieldMap) {
        Map<String, Field> dbName2FieldMap = new HashMap<>();
        Lists.newArrayList(dbClassFields).forEach(f -> dbName2FieldMap.put(f.getName(), f));

        for( Field f : vmClassFields ) {
            MapDbModelField fieldAnnotation = f.getAnnotation(MapDbModelField.class);
            if( fieldAnnotation != null ) {
                if( fieldAnnotation.fieldName().isEmpty() ) {

                    fieldMap.put(f.getName(), dbName2FieldMap.get(f.getName()));
                }
                else
                {
                    fieldMap.put(f.getName(), dbName2FieldMap.get(fieldAnnotation.fieldName()));
                }
            }
        }
    }



}
