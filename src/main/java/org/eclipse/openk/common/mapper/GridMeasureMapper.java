/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.common.mapper;

import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.api.HGridMeasure;
import org.eclipse.openk.api.PowerSystemResource;
import org.eclipse.openk.api.SingleGridmeasure;
import org.eclipse.openk.api.Steps;
import org.eclipse.openk.common.mapper.generic.GenericApiToDbMapper;
import org.eclipse.openk.db.dao.TblStepsDao;
import org.eclipse.openk.db.model.HTblGridMeasure;
import org.eclipse.openk.db.model.TblGridMeasure;
import org.eclipse.openk.db.model.TblSingleGridmeasure;
import org.eclipse.openk.db.model.TblSteps;

import java.util.ArrayList;
import java.util.List;


public class GridMeasureMapper {


    public void adjustPowerSystemResourceMapFromViewModel(SingleGridmeasure sg, TblSingleGridmeasure mSg) {
      if (sg.getPowerSystemResource() != null && sg.getPowerSystemResource().getCimId() != null){
        mSg.setCimId(sg.getPowerSystemResource().getCimId());
        mSg.setCimName(sg.getPowerSystemResource().getCimName());
        mSg.setCimDescription(sg.getPowerSystemResource().getCimDescription());
      }
    }

    private void adjustPowerSystemResourceMapToViewModel(TblSingleGridmeasure sg, SingleGridmeasure vmSg) {
      PowerSystemResource psr = new PowerSystemResource();
      psr.setCimId(sg.getCimId());
      psr.setCimName(sg.getCimName());
      psr.setCimDescription(sg.getCimDescription());
      vmSg.setPowerSystemResource(psr);
    }

    public TblGridMeasure mapFromVModel(GridMeasure vmGridMeasure) {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        // do extra mappings here!
        return mapper.mapFromViewModel(TblGridMeasure.class, vmGridMeasure);
    }

    public GridMeasure mapToVModel( TblGridMeasure tblGridmeasure ) {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        // do extra mappings here!
        return mapper.mapToViewModel(GridMeasure.class, tblGridmeasure);
    }

    public GridMeasure mapToVModel(TblGridMeasure tblGridmeasure, List<TblSingleGridmeasure> listSingleGridmeasures, TblStepsDao stepsDao) {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
         // do extra mappings here!
        GridMeasure vmGridMeasure = mapper.mapToViewModel(GridMeasure.class, tblGridmeasure);
        List <SingleGridmeasure> listVmSingleGridmeasures= new ArrayList<>();

        for (TblSingleGridmeasure sg : listSingleGridmeasures) {
            processSingleGridMeasure(stepsDao, listVmSingleGridmeasures, sg);
        }
        vmGridMeasure.setListSingleGridmeasures(listVmSingleGridmeasures);
        return vmGridMeasure;
    }

    private void processSingleGridMeasure(TblStepsDao stepsDao, List<SingleGridmeasure> listVmSingleGridmeasures, TblSingleGridmeasure sg) {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        SingleGridmeasure vmSg = mapper.mapToViewModel(SingleGridmeasure.class, sg);
        List <Steps> listVmSteps= new ArrayList<>();
        if (sg.getCimId() != null){
          adjustPowerSystemResourceMapToViewModel(sg, vmSg);
        }

        List<TblSteps> listTblSteps = stepsDao.getStepsBySingleGmIdInTx(sg.getId());

        if (listTblSteps != null) {

            for (TblSteps stp : listTblSteps) {
                    Steps vmStp = mapper.mapToViewModel(Steps.class, stp);
                    listVmSteps.add(vmStp);
            }
            vmSg.setListSteps(listVmSteps);
        }

        listVmSingleGridmeasures.add(vmSg);
    }

    public HGridMeasure mapToVModel(HTblGridMeasure htblGridmeasure) {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        // do extra mappings here!
        return mapper.mapToViewModel(HGridMeasure.class, htblGridmeasure);
    }


}
