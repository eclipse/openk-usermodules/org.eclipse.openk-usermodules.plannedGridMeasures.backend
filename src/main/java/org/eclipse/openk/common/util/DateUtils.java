/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.common.util;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

  private DateUtils(){

  }

  public static final Logger LOGGER = Logger.getLogger(DateUtils.class.getName());

  private static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";

  public static String formatDateToString(Date date){
    if (date == null) {
      return null;
    }
    DateFormat dateFormat = new SimpleDateFormat(DATEFORMAT, Locale.GERMANY);
    return dateFormat.format(date);
  }

  public static Date parseStringToDate(String date){
    try {
      if (date == null) {
        return null;
      }
      DateFormat dateFormat = new SimpleDateFormat(DATEFORMAT, Locale.GERMANY);
      dateFormat.setLenient( false );
      return dateFormat.parse(date);
    } catch (ParseException e) {
      LOGGER.error("Error in formatStringToDate while parsing date: " + date);
    }
    return null;
  }

  public static LocalDate asLocalDate(Date date) {
    return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.of(Globals.DATE_ZONE_ID_EUROPE)).toLocalDate();
  }

  public static LocalDateTime asLocalDateTime(Date date) {
    return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.of(Globals.DATE_ZONE_ID_EUROPE)).toLocalDateTime();
  }

}
