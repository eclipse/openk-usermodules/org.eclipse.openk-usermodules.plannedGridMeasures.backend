/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import org.eclipse.openk.db.model.HTblGridMeasure;
import org.eclipse.openk.db.model.TblGridMeasure;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class HTblGridMeasureDao extends GenericDaoJpa<TblGridMeasure, Integer>{
    public HTblGridMeasureDao() {
        super();
    }

    public HTblGridMeasureDao(EntityManager em) {
        super(em);
    }

    public List<HTblGridMeasure> getHistoricalGridMeasuresByIdInTx(Integer gmId) {
        try {
            String selectString = "from HTblGridMeasure t WHERE t.id = :gmId ORDER BY t.modDate DESC";
            Query q = getEM().createQuery(selectString);
            q.setParameter("gmId", gmId);
            return (List<HTblGridMeasure>) q.getResultList();
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

}
