/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import org.eclipse.openk.db.model.TblSingleGridmeasure;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class TblSingleGridmeasureDao extends GenericDaoJpa<TblSingleGridmeasure, Integer>{
    public TblSingleGridmeasureDao() {
        super();
    }

    public TblSingleGridmeasureDao(EntityManager em) {
        super(em);
    }

    public List<TblSingleGridmeasure> getSingleGridmeasuresInTx() {
        try {
            String selectString = "from TblSingleGridmeasure t";
            Query q = getEM().createQuery(selectString);

            return refreshModelCollection(q.getResultList());
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<TblSingleGridmeasure> getSingleGridmeasuresByGmIdInTx(Integer gmId) {
        try {
            String selectString = "from TblSingleGridmeasure t WHERE t.fkTblGridmeasure = :gmId ORDER BY t.sortorder";
            Query q = getEM().createQuery(selectString);
            q.setParameter("gmId", gmId);

            return q.getResultList();
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null; // NOSONAR We decide to use null logic
        }
    }

    public List<String> getResponsiblesOnSite() {
        List<String> retList = new ArrayList<>();
        try {
            String selectString = "select t.responsibleOnSiteName from TblSingleGridmeasure t";
            Query q = getEM().createQuery(selectString);

            return (List<String>)q.getResultList();

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
        }
        return retList;
    }



    public List<String> getNetworkControls() {
        List<String> retList = new ArrayList<>();
        try {
            String selectString = "select t.networkControl from TblSingleGridmeasure t";
            Query q = getEM().createQuery(selectString);

            return (List<String>)q.getResultList();

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
        }
        return retList;
    }

    public List<String> getUserDepartmentsResponsibleOnSite() {
        List<String> retList = new ArrayList<>();
        try {
            String selectString = "select t.responsibleOnSiteDepartment from TblSingleGridmeasure t";
            Query q = getEM().createQuery(selectString);

            return (List<String>)q.getResultList();

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
        }
        return retList;
    }

}

