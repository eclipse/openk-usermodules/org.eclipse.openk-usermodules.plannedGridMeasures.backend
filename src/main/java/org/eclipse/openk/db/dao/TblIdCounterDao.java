/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.http.HttpStatus;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.model.TblIdCounter;
import org.eclipse.openk.db.model.TblLock;
import org.eclipse.openk.db.model.TblMeasureDocuments;


public class TblIdCounterDao extends GenericDaoJpa<TblIdCounter, Integer>{
    public TblIdCounterDao() {
        super();
    }

    public TblIdCounterDao(EntityManager em) {
        super(em);
    }

    public TblIdCounter getIdCounterForCounterType(String counterType) throws HttpStatusException {
        try {
            String selectString = "from TblIdCounter t WHERE t.counterType  = :counterType";
            Query q = getEM().createQuery(selectString);
            q.setParameter("counterType",counterType);
            if(!q.getResultList().isEmpty()) {
                return (TblIdCounter) q.getSingleResult();
            }
            else {
                return null;
            }

        }
        catch (Exception t) {
            LOGGER.error("Error in getIdCounterForCounterType",t);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR, t.getMessage());
        }
    }
}
