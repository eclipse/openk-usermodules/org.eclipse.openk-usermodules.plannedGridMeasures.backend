/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityHelper {
    private static String factoryName = null;
    private static Map<String, Object> properties = null;
    private static EntityManagerFactory entityManagerFactory;

    private EntityHelper() {
    }

    public static synchronized void setProperties( Map< String, Object > properties) {
        EntityHelper.properties = properties;
    }

    public static synchronized void setFactoryName( String factoryName ) {
        EntityHelper.factoryName=factoryName;
    }

    public static synchronized EntityManagerFactory getEMF() {
        if (factoryName == null  ) {
            throw new RuntimeException("Entity factory has not been initialized with a factory name!"); // NOSONAR
        }

        if (entityManagerFactory == null) {
            entityManagerFactory = createFactory();
        }
        return entityManagerFactory;
    }

    protected static EntityManagerFactory createFactory() {
        if( properties == null ) {
            return Persistence.createEntityManagerFactory(factoryName);
        }
        else
        {
            return Persistence.createEntityManagerFactory(factoryName, properties);
        }
    }
}
