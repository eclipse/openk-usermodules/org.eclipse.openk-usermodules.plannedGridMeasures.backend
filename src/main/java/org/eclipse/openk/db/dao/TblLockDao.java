/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import org.eclipse.openk.db.model.TblLock;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

public class TblLockDao extends GenericDaoJpa<TblLock, Integer>{
    public TblLockDao() {
        super();
    }

    public TblLockDao(EntityManager em) {
        super(em);
    }

    public List<TblLock> getLocksInTx() {
        try {
            String selectString = "from TblLock t";
            Query q = getEM().createQuery(selectString);
            return (List<TblLock>) q.getResultList();
        } catch (Exception t) {
            LOGGER.error("Caught exception", t);
            return new LinkedList<>();
        }
    }

    public TblLock getLock(Integer key, String info) {
        try {
            String selectString = "from TblLock t WHERE t.key = :key and t.info = :info";
            Query q = getEM().createQuery(selectString);
            q.setParameter("key", key);
            q.setParameter("info", info);

            if(!q.getResultList().isEmpty()) {
                return (TblLock) q.getSingleResult();
            }
            else {
                return null;
            }
        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            return null;
        }
    }
}
