/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao.interfaces;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;

public interface IGenericDao<T, I extends Serializable> {


    T findById(Class<T> persistentClass, I id);

    T findByIdInTx(Class<T> persistentClass, I id);

    T store(T persistentObject) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    T storeInTx(T persistentObject) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    T persist(T persistentObject) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    T persistInTx(T persistentObject) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    T merge(T persistentObject) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    T mergeInTx(T persistentObject) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    void remove(T persistentObject, I id) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    void removeInTx(T persistentObject, I id) throws Exception; // NOSONAR _fd Legacy Code! Don't touch!

    List<T> find(boolean all, int maxResult, int firstResult);

    List<T> findInTx(boolean all, int maxResult, int firstResult);

    EntityManager getEM();

    void startTransaction();

    void endTransaction();

    void commitTransaction();

    void rollbackTransaction();

    void closeSession();
}





























