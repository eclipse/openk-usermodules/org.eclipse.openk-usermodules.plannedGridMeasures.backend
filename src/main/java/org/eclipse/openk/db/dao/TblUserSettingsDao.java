/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.dao;

import org.apache.http.HttpStatus;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.db.model.TblUserSettings;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.Instant;
import java.util.Date;

public class TblUserSettingsDao extends GenericDaoJpa<TblUserSettings, Integer>{
    public TblUserSettingsDao() {
        super();
    }

    public TblUserSettingsDao(EntityManager em) {
        super(em);
    }

    public TblUserSettings getUserSettingsBySettingType(String username, String settingType) throws HttpStatusException{
        try {
            String selectString = "from TblUserSettings t WHERE t.username = :username and t.settingType = :settingType";
            Query q = getEM().createQuery(selectString);
            q.setParameter("username",username);
            q.setParameter("settingType",settingType);

            if(!q.getResultList().isEmpty()) {
                return (TblUserSettings) q.getSingleResult();
            }
            else {
                String errText = "No Result available for the given username and settingType";
                LOGGER.info(errText);
                return new TblUserSettings();
            }

        } catch (Exception t) {
            LOGGER.error(t.getMessage());
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR, t.getMessage());
        }
    }

    public TblUserSettings updateUserSettingsInDB(TblUserSettings tblUsUpdate, TblUserSettings tblUsDB)  throws HttpStatusException{

        tblUsUpdate.setCreateDate(tblUsDB.getCreateDate());
        tblUsUpdate.setCreateUser(tblUsDB.getCreateUser());
        tblUsUpdate.setId(tblUsDB.getId());

        try {
            return this.storeInTx(tblUsUpdate);

        } catch (Exception e) {
            String errorText = tblUsUpdate != null ? // NOSONAR
                    "Error storing userSettings:" + JsonGeneratorBase.getGson().toJson(tblUsUpdate) :
                    "Cannot store empty userSettings";

            LOGGER.error(errorText, e);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }

    public TblUserSettings createUserSettingsInDb(String changeUser, TblUserSettings tblUs) throws HttpStatusException{
        try {
            tblUs.setCreateUser(changeUser);
            tblUs.setCreateDate(Date.from(Instant.now()));
            return this.storeInTx(tblUs);

        } catch (Exception e) {
            String errorText = tblUs != null ? // NOSONAR Sonar is too eager here! Condition not always true
                    "Error storing UserSettings Entity:" + JsonGeneratorBase.getGson().toJson(tblUs) :
                    "Cannot store empty UserSettings";
            LOGGER.error(errorText, e);
            throw new HttpStatusException(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
