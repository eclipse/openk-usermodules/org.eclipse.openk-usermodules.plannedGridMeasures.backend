/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
/**
 * The persistent class for the "TBL_SINGLE_GRIDMEASURE" database table.
 */
@Entity
@Table(name = "TBL_SINGLE_GRIDMEASURE", schema = "public")
@NamedQuery(name = "TblSingleGridmeasure.findAll", query = "SELECT t FROM TblSingleGridmeasure t")
public class TblSingleGridmeasure implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBL_SINGLE_GRIDMEASURE_ID_SEQ")
    @SequenceGenerator(name = "TBL_SINGLE_GRIDMEASURE_ID_SEQ", sequenceName = "TBL_SINGLE_GRIDMEASURE_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(name = "sortorder")
    private Integer sortorder;

    @Column(name = "title")
    private String title;

    @Column(name = "switching_object")
    private String switchingObject;

    @Column(name = "cim_id")
    private String cimId;

    @Column(name = "cim_name")
    private String cimName;

    @Column(name = "cim_description")
    private String cimDescription;

    @Column(name = "planned_starttime_singlemeasure")
    private Date plannedStarttimeSinglemeasure;

    @Column(name = "planned_endtime_singlemeasure")
    private Date plannedEndtimeSinglemeasure;

    @Column (name= "description")
    private String description;

    @Column (name= "responsible_onsite_name")
    private String responsibleOnSiteName;

    @Column (name= "responsible_onsite_department")
    private String responsibleOnSiteDepartment;

    @Column (name= "network_control")
    private String networkControl;

    @Column (name= "fk_tbl_gridmeasure")
    private Integer fkTblGridmeasure;

    @Column (name= "create_user")
    private String createUser;

    @Column (name= "create_date")
    private Date createDate;

    @Column (name= "mod_user")
    private String modUser;

    @Column (name= "mod_date")
    private Date modDate;



    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getSortorder() { return sortorder; }

    public void setSortorder(Integer sortorder) { this.sortorder = sortorder; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getSwitchingObject() { return switchingObject; }

    public void setSwitchingObject(String affectedResource) { this.switchingObject = affectedResource; }

    public String getCimId() { return cimId; }

    public void setCimId(String cimId) { this.cimId = cimId; }

    public String getCimName() { return cimName; }

    public void setCimName(String cimName) { this.cimName = cimName; }

    public String getCimDescription() { return cimDescription; }

    public void setCimDescription(String cimDescription) { this.cimDescription = cimDescription; }

    public Date getPlannedStarttimeSinglemeasure() { return plannedStarttimeSinglemeasure; }

    public void setPlannedStarttimeSinglemeasure(Date plannedStarttimeSinglemeasure) { this.plannedStarttimeSinglemeasure = plannedStarttimeSinglemeasure; }

    public Date getPlannedEndtimeSinglemeasure() { return plannedEndtimeSinglemeasure; }

    public void setPlannedEndtimeSinglemeasure(Date plannedEndtimeSinglemeasure) { this.plannedEndtimeSinglemeasure = plannedEndtimeSinglemeasure; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public static long getSerialVersionUID() { return serialVersionUID; }

    public String getResponsibleOnSiteName() { return responsibleOnSiteName; }

    public void setResponsibleOnSiteName(String responsibleOnSiteName) { this.responsibleOnSiteName = responsibleOnSiteName; }

    public String getResponsibleOnSiteDepartment() { return responsibleOnSiteDepartment; }

    public void setResponsibleOnSiteDepartment(String responsibleOnSiteDepartment) { this.responsibleOnSiteDepartment = responsibleOnSiteDepartment; }

    public String getNetworkControl() { return networkControl; }

    public void setNetworkControl(String networkControl) { this.networkControl = networkControl; }

    public Integer getFkTblGridmeasure() { return fkTblGridmeasure; }

    public void setFkTblGridmeasure(Integer fkTblGridmeasure) { this.fkTblGridmeasure = fkTblGridmeasure; }

    public String getCreateUser() { return createUser; }

    public void setCreateUser(String createUser) { this.createUser = createUser; }

    public Date getCreateDate() { return createDate; }

    public void setCreateDate(Date createDate) { this.createDate = createDate; }

    public String getModUser() { return modUser; }

    public void setModUser(String modUser) { this.modUser = modUser; }

    public Date getModDate() { return modDate; }

    public void setModDate(Date modDate) { this.modDate = modDate; }
}

