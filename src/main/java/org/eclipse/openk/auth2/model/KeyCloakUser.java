/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.auth2.model;

import java.util.List;

public class KeyCloakUser {
    private String id;
    private long createdTimestamp;
    private String username;
    private boolean enabled;
    private boolean totp;
    private boolean emailVerified;
    private String firstName;
    private String lastName;
    private List<String> realmRoles;

    private List<String> disableableCredentialTypes;
    private List<String> requiredActions;
    private KeyCloakUserAccess access;

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }

    public long getCreatedTimestamp() { return createdTimestamp; }
    public void setCreatedTimestamp(long createdTimestamp) { this.createdTimestamp = createdTimestamp; }

    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }

    public boolean getEnabled() { return enabled; }
    public void setEnabled(boolean enabled) { this.enabled = enabled; }

    public boolean getTotp() { return totp; }
    public void setTotp(boolean totp) { this.totp = totp; }

    public boolean getEmailVerified() { return emailVerified; }
    public void setEmailVerified(boolean emailVerified) { this.emailVerified = emailVerified; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public List<String> getDisableableCredentialTypes() {
        return disableableCredentialTypes;
    }
    public void setDisableableCredentialTypes(List<String> disableableCredentialTypes) { this.disableableCredentialTypes = disableableCredentialTypes; }

    public List<String> getRequiredActions() {
        return requiredActions;
    }
    public void setRequiredActions(List<String> requiredActions) { this.requiredActions = requiredActions; }

    public KeyCloakUserAccess getAccess() { return access; }
    public void setAccess(KeyCloakUserAccess access) { this.access = access; }

    public List<String> getRealmRoles() {
        return realmRoles;
    }

    public void setRealmRoles(List<String> realmRoles) {
        this.realmRoles = realmRoles;
    }
}
