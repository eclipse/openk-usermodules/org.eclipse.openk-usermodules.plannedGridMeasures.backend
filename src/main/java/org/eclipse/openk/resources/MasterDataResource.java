/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.resources;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.eclipse.openk.api.*;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.controller.MasterDataBackendController;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(value = "/mics/gridmeasures")
@ApiResponses( value ={
        @ApiResponse(code = 200, message = "OK",response = VersionInfo.class,reference = "#/definitions/VersionInfo"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 404, message = "Not found"),
        @ApiResponse(code = 423, message = "Locked"),
        @ApiResponse(code = 500, message = "Internal Server Error") } )
@Path("/mics/gridmeasures")
public class MasterDataResource extends BaseResource {

    private static final Logger LOGGER = Logger.getLogger(MasterDataResource.class.getName());

    public MasterDataResource() {
        super(LOGGER);
    }

    @ApiOperation(value = "EmailAddresses", notes = "This service retrieves a list of email-addresses which are configured in the relevant email-templates")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = String.class) } )
    @GET
    @Path("/getEmailAddressesFromTemplates")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getEmailAddressesFromTemplates(@ApiParam(name ="Authorization", value ="JWT Token", required =true)@HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
                .getEmailAddressesFromTemplates());
    }

    @ApiOperation(value = "Branches", notes = "This service gets all Branches from the Database")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = Branch.class,reference = "#/definitions/Branches") } )
    @GET
    @Path("/getBranches")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getBranches(@ApiParam(name ="Authorization", value ="JWT Token", required =true)@HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
                .getBranches());
    }

    @ApiOperation(value = "BranchLevels", notes = "This service gets all BranchLevels for a certain branch from the Database")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = BranchLevel.class,reference = "#/definitions/BranchLevel") } )
    @GET
    @Path("/getBranchLevelsByBranch/{branchId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getBranchLevelsByBranch(@ApiParam(name ="Authorization", value ="JWT Token", required =true)
                                   @HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt,
                                   @ApiParam(name ="id", value ="id", required =true)
                                   @PathParam("branchId") String branchId) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
                .getBranchLevelsByBranch(Integer.parseInt(branchId)));
    }

    @ApiOperation(value = "GmStatus", notes = "This service gets all possible Status from the Database")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = GmStatus.class,reference = "#/definitions/GmStatus") } )
    @GET
    @Path("/getGmStatus")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getGmStatus(@ApiParam(name ="Authorization", value ="JWT Token", required =true)@HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
                .getGmStatus());
    }

    @ApiOperation(value = "CostCenter", notes = "This service gets all possible Cost-Centers from the Database")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = CostCenter.class,reference = "#/definitions/CostCenter") } )
    @GET
    @Path("/getCostCenters")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getCostCenters(@ApiParam(name ="Authorization", value ="JWT Token", required =true)@HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
                .getCostCenters());
    }



    @ApiOperation(value = "Territory", notes = "This service retrieves all territories as Strings from the Database")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = Territory.class,reference = "#/definitions/Territory") } )
    @GET
    @Path("/getTerritories")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getTerritories(@ApiParam(name ="Authorization", value ="JWT Token", required =true)@HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
                .getTerritories());
    }

    @ApiOperation(value = "UserSettings", notes = "This service gets all User-Settings for the given Username from the Database")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = UserSettings.class,reference = "#/definitions/UserSettings") } )
    @GET
    @Path("/getUserSettings/{settingType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getUserSettings(@PathParam("settingType") String settingType,
        @ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
            .getUserSettings(modusr, settingType));
    }

    @ApiOperation(value = "Store User Setting", notes = "This service stores the given User Setting in the "+
            "database.")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = UserSettings.class,reference = "#/definitions/UserSettings") } )
    @PUT
    @Path("/storeUserSettings")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response storeUserSettings(@ApiParam(name ="Authorization", value ="JWT Token", required =true)@HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt,
                                     String settingsString) {
        UserSettings userSettings = JsonGeneratorBase.getGson().fromJson(settingsString, UserSettings.class);
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
                .storeUserSettings(modusr, userSettings));
    }


    @ApiOperation(value = "BackendSettings", notes = "This service gets the Backendsettings")
    @ApiResponses( value ={ @ApiResponse(code = 200, message = "OK", response = BackendSettings.class,reference = "#/definitions/BackendSettings") } )
    @GET
    @Path("/getBackendSettings")
    @Produces(MediaType.APPLICATION_JSON)
    @Timed
    public Response getBackendSettings(@ApiParam(name = "Authorization", value = "JWT Token", required = true)
    @HeaderParam(Globals.KEYCLOAK_AUTH_TAG) String jwt) {
        return invokeRunnable(jwt, SecureType.NORMAL, modusr -> new MasterDataBackendController()
            .getBackendSettings());
    }


}
