/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.api.mail;

public class EmailTemplatePaths {

  private String appliedEmailTemplate;
  private String forapprovalEmailTemplate;
  private String approvedEmailTemplate;
  private String requestedEmailTemplate;
  private String releasedEmailTemplate;
  private String activeEmailTemplate;
  private String inworkEmailTemplate;
  private String workfinishedEmailTemplate;
  private String finishedEmailTemplate;
  private String closedEmailTemplate;
  private String cancelledEmailTemplate;
  private String rejectedEmailTemplate;

  public String getAppliedEmailTemplate() {
    return appliedEmailTemplate;
  }

  public void setAppliedEmailTemplate(String appliedEmailTemplate) {
    this.appliedEmailTemplate = appliedEmailTemplate;
  }

  public String getForapprovalEmailTemplate() {
    return forapprovalEmailTemplate;
  }

  public void setForapprovalEmailTemplate(String forapprovalEmailTemplate) {
    this.forapprovalEmailTemplate = forapprovalEmailTemplate;
  }

  public String getApprovedEmailTemplate() {
    return approvedEmailTemplate;
  }

  public void setApprovedEmailTemplate(String approvedEmailTemplate) {
    this.approvedEmailTemplate = approvedEmailTemplate;
  }

  public String getRequestedEmailTemplate() {
    return requestedEmailTemplate;
  }

  public void setRequestedEmailTemplate(String requestedEmailTemplate) {
    this.requestedEmailTemplate = requestedEmailTemplate;
  }

  public String getReleasedEmailTemplate() {
    return releasedEmailTemplate;
  }

  public void setReleasedEmailTemplate(String releasedEmailTemplate) {
    this.releasedEmailTemplate = releasedEmailTemplate;
  }

  public String getActiveEmailTemplate() {
    return activeEmailTemplate;
  }

  public void setActiveEmailTemplate(String activeEmailTemplate) {
    this.activeEmailTemplate = activeEmailTemplate;
  }

  public String getInworkEmailTemplate() {
    return inworkEmailTemplate;
  }

  public void setInworkEmailTemplate(String inworkEmailTemplate) {
    this.inworkEmailTemplate = inworkEmailTemplate;
  }

  public String getWorkfinishedEmailTemplate() {
    return workfinishedEmailTemplate;
  }

  public void setWorkfinishedEmailTemplate(String workfinishedEmailTemplate) {
    this.workfinishedEmailTemplate = workfinishedEmailTemplate;
  }

  public String getFinishedEmailTemplate() {
    return finishedEmailTemplate;
  }

  public void setFinishedEmailTemplate(String finishedEmailTemplate) {
    this.finishedEmailTemplate = finishedEmailTemplate;
  }

  public String getClosedEmailTemplate() {
    return closedEmailTemplate;
  }

  public void setClosedEmailTemplate(String closedEmailTemplate) {
    this.closedEmailTemplate = closedEmailTemplate;
  }

  public String getCancelledEmailTemplate() {
    return cancelledEmailTemplate;
  }

  public void setCancelledEmailTemplate(String cancelledEmailTemplate) {
    this.cancelledEmailTemplate = cancelledEmailTemplate;
  }

  public String getRejectedEmailTemplate() {
    return rejectedEmailTemplate;
  }

  public void setRejectedEmailTemplate(String rejectedEmailTemplate) {
    this.rejectedEmailTemplate = rejectedEmailTemplate;
  }


}
