/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelClass;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelField;
import org.eclipse.openk.db.model.HTblGridMeasure;

import java.util.Date;
import java.util.List;

@MapDbModelClass(classType = HTblGridMeasure.class)
public class HGridMeasure {

    @JsonProperty
    @MapDbModelField
    private Integer hId;

    @JsonProperty
    @MapDbModelField
    private Integer hAction;

    @JsonProperty
    @MapDbModelField
    private Date hDate;

    @JsonProperty
    @MapDbModelField
    private String hUser;

    @JsonProperty
    @MapDbModelField
    private Integer id;

    @JsonProperty
    @MapDbModelField( fieldName = "idDescriptive")
    private String descriptiveId;

    @JsonProperty
    @MapDbModelField
    private String title;

    @JsonProperty
    @MapDbModelField
    private String affectedResource;

    @JsonProperty
    @MapDbModelField
    private String remark;

    @JsonProperty
    @MapDbModelField
    private String createUser;

    @JsonProperty
    @MapDbModelField
    private String createUserDepartment;

    @JsonProperty
    @MapDbModelField
    private Date createDate;

    @JsonProperty
    @MapDbModelField
    private String modUser;

    @JsonProperty
    @MapDbModelField
    private String modUserDepartment;

    @JsonProperty
    @MapDbModelField
    private Date modDate;

    @JsonProperty
    @MapDbModelField( fieldName = "fkRefGmStatus")
    private Integer statusId;

    @JsonProperty
    @MapDbModelField
    private String switchingObject;

    @JsonProperty
    @MapDbModelField
    private String costCenter;

    @JsonProperty
    @MapDbModelField
    private String approvalBy;

    @JsonProperty
    @MapDbModelField
    private String areaOfSwitching;

    @JsonProperty
    @MapDbModelField
    private String appointmentRepetition;

    @JsonProperty
    @MapDbModelField
    private Date appointmentStartdate;

    @JsonProperty
    @MapDbModelField
    private Integer appointmentNumberOf;

    @JsonProperty
    @MapDbModelField
    private Date plannedStarttimeFirstSinglemeasure;


    @JsonProperty
    @MapDbModelField
    private Date endtimeGridmeasure;

    @JsonProperty
    @MapDbModelField
    private String timeOfReallocation;

    @JsonProperty
    @MapDbModelField
    private String description;

    @JsonProperty
    @MapDbModelField( fieldName = "fkRefBranch")
    private Integer branchId;

    @JsonProperty
    @MapDbModelField( fieldName = "fkRefBranchLevel")
    private Integer branchLevelId;

    @JsonProperty
    private List<SingleGridmeasure> listSingleGridmeasures;


    public HGridMeasure() {
        // default Constructor
    }

    public Integer getHId() {
        return hId;
    }

    public void setHId(Integer hId) {
        this.hId = hId;
    }

    public Integer getHAction() {
        return hAction;
    }

    public void setHAction(Integer haction) {
        this.hAction = haction;
    }

    public Date getHDate() {
        return hDate;
    }

    public void setHDate(Date hdate) {
        this.hDate = hdate;
    }

    public String getHUser() {
        return hUser;
    }

    public void setHUser(String huser) {
        this.hUser = huser;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescriptiveId() { return descriptiveId; }

    public void setDescriptiveId(String descriptiveId) { this.descriptiveId = descriptiveId; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getAffectedResource() { return affectedResource; }

    public void setAffectedResource(String affectedResource) { this.affectedResource = affectedResource; }

    public String getRemark() { return remark; }

    public void setRemark(String remark) { this.remark = remark; }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) { this.statusId = statusId; }

    public String getSwitchingObject() { return switchingObject; }

    public void setSwitchingObject(String switchingObject) { this.switchingObject = switchingObject; }

    public String getCostCenter() { return costCenter; }

    public void setCostCenter(String costCenter) { this.costCenter = costCenter; }

    public String getApprovalBy() { return approvalBy; }

    public void setApprovalBy(String approvalBy) { this.approvalBy = approvalBy; }

    public String getAreaOfSwitching() { return areaOfSwitching; }

    public void setAreaOfSwitching(String areaOfSwitching) { this.areaOfSwitching = areaOfSwitching; }

    public String getAppointmentRepetition() { return appointmentRepetition; }

    public void setAppointmentRepetition(String appointmentRepetition) { this.appointmentRepetition = appointmentRepetition; }

    public Date getAppointmentStartdate() { return appointmentStartdate; }

    public void setAppointmentStartdate(Date appointmentStartdate) { this.appointmentStartdate = appointmentStartdate; }

    public Integer getAppointmentNumberOf() { return appointmentNumberOf; }

    public void setAppointmentNumberOf(Integer appointmentNumberOf) { this.appointmentNumberOf = appointmentNumberOf; }

    public Date getPlannedStarttimeFirstSinglemeasure() { return plannedStarttimeFirstSinglemeasure; }

    public void setPlannedStarttimeFirstSinglemeasure(Date plannedStarttimeFirstSinglemeasure) { this.plannedStarttimeFirstSinglemeasure = plannedStarttimeFirstSinglemeasure; }

    public Date getEndtimeGridmeasure() { return endtimeGridmeasure; }

    public void setEndtimeGridmeasure(Date endtimeGridmeasure) { this.endtimeGridmeasure = endtimeGridmeasure; }

    public String getTimeOfReallocation() { return timeOfReallocation; }

    public void setTimeOfReallocation(String timeOfReallocation) { this.timeOfReallocation = timeOfReallocation; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public Integer getBranchId() { return branchId; }

    public void setBranchId(Integer branchId) { this.branchId = branchId; }

    public Integer getBranchLevelId() { return branchLevelId; }

    public void setBranchLevelId(Integer branchLevelId) { this.branchLevelId = branchLevelId; }

    public String getCreateUser() { return createUser; }

    public void setCreateUser(String createUser) { this.createUser = createUser; }

    public String getCreateUserDepartment() { return createUserDepartment; }

    public void setCreateUserDepartment(String createUserDepartment) { this.createUserDepartment = createUserDepartment; }

    public Date getCreateDate() { return createDate; }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModUser() {
        return modUser;
    }

    public void setModUser(String modUser) {
        this.modUser = modUser;
    }

    public String getModUserDepartment() { return modUserDepartment; }

    public void setModUserDepartment(String modUserDepartment) { this.modUserDepartment = modUserDepartment; }

    public Date getModDate() { return modDate; }

    public void setModDate(Date modDate) { this.modDate = modDate; }

    public List<SingleGridmeasure> getListSingleGridmeasures() { return listSingleGridmeasures; }

    public void setListSingleGridmeasures(List<SingleGridmeasure> listSingleGridmeasures) { this.listSingleGridmeasures = listSingleGridmeasures; }
}
