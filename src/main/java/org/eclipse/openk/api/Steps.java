/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelClass;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelField;
import org.eclipse.openk.db.model.TblSteps;

@MapDbModelClass(classType = TblSteps.class)
public class Steps {

    @JsonProperty
    @MapDbModelField
    private Integer id;

    @JsonProperty
    @MapDbModelField
    private Integer sortorder;

    @JsonProperty
    @MapDbModelField
    private String switchingObject;

    @JsonProperty
    @MapDbModelField
    private String type;

    @JsonProperty
    @MapDbModelField
    private String presentTime;

    @JsonProperty
    @MapDbModelField
    private String presentState;

    @JsonProperty
    @MapDbModelField
    private String targetState;

    @JsonProperty
    @MapDbModelField
    private String operator;

    @JsonProperty
    @MapDbModelField( fieldName = "fkTblSingleGridmeasure")
    private Integer singleGridmeasureId;

    @JsonProperty
    private boolean delete;



    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getSortorder() { return sortorder; }

    public void setSortorder(Integer sortorder) { this.sortorder = sortorder; }

    public String getSwitchingObject() { return switchingObject; }

    public void setSwitchingObject(String switchingObject) { this.switchingObject = switchingObject; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getPresentTime() { return presentTime; }

    public void setPresentTime(String presentTime) { this.presentTime = presentTime; }

    public String getPresentState() { return presentState; }

    public void setPresentState(String presentState) { this.presentState = presentState; }

    public String getOperator() { return operator; }

    public void setOperator(String operator) { this.operator = operator; }

    public String getTargetState() {
        return targetState;
    }

    public void setTargetState(String targetState) {
        this.targetState = targetState;
    }

    public Integer getSingleGridmeasureId() {
        return singleGridmeasureId;
    }

    public void setSingleGridmeasureId(Integer singleGridmeasureId) {
        this.singleGridmeasureId = singleGridmeasureId;
    }

    public boolean isDelete() { return delete; }

    public void setDelete(boolean delete) { this.delete = delete; }

}

