/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelClass;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelField;
import org.eclipse.openk.db.model.TblSingleGridmeasure;

import java.util.Date;
import java.util.List;

@MapDbModelClass(classType = TblSingleGridmeasure.class)
public class SingleGridmeasure {

    @JsonProperty
    @MapDbModelField
    private Integer id;

    @JsonProperty
    @MapDbModelField
    private Integer sortorder;

    @JsonProperty
    @MapDbModelField
    private String title;

    @JsonProperty
    @MapDbModelField
    private String switchingObject;

    @JsonProperty
    private PowerSystemResource powerSystemResource;

    @JsonProperty
    @MapDbModelField
    private Date plannedStarttimeSinglemeasure;

    @JsonProperty
    @MapDbModelField
    private Date plannedEndtimeSinglemeasure;

    @JsonProperty
    @MapDbModelField
    private String description;

    @JsonProperty
    @MapDbModelField
    private String responsibleOnSiteName;

    @JsonProperty
    @MapDbModelField
    private String responsibleOnSiteDepartment;

    @JsonProperty
    @MapDbModelField
    private String networkControl;

    @JsonProperty
    @MapDbModelField( fieldName = "fkTblGridmeasure")
    private Integer gridmeasureId;

    @JsonProperty
    private List<Steps> listSteps;

    @JsonProperty
    private boolean delete;



    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public Integer getSortorder() { return sortorder; }

    public void setSortorder(Integer sortorder) { this.sortorder = sortorder; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getSwitchingObject() { return switchingObject; }

    public void setSwitchingObject(String switchingObject) { this.switchingObject = switchingObject; }

    public Date getPlannedStarttimeSinglemeasure() { return plannedStarttimeSinglemeasure; }

    public void setPlannedStarttimeSinglemeasure(Date plannedStarttimeSinglemeasure) { this.plannedStarttimeSinglemeasure = plannedStarttimeSinglemeasure; }

    public Date getPlannedEndtimeSinglemeasure() { return plannedEndtimeSinglemeasure; }

    public void setPlannedEndtimeSinglemeasure(Date plannedEndtimeSinglemeasure) { this.plannedEndtimeSinglemeasure = plannedEndtimeSinglemeasure; }

    public String getDescription() { return description; }

    public void setDescription(String dscription) { this.description = dscription; }

    public String getResponsibleOnSiteName() { return responsibleOnSiteName; }

    public void setResponsibleOnSiteName(String responsibleOnSiteName) { this.responsibleOnSiteName = responsibleOnSiteName; }

    public String getResponsibleOnSiteDepartment() { return responsibleOnSiteDepartment; }

    public void setResponsibleOnSiteDepartment(String responsibleOnSiteDepartment) { this.responsibleOnSiteDepartment = responsibleOnSiteDepartment; }

    public String getNetworkControl() { return networkControl; }

    public void setNetworkControl(String networkControl) { this.networkControl = networkControl; }

    public Integer getGridmeasureId() { return gridmeasureId; }

    public void setGridmeasureId(Integer gridmeasureId) { this.gridmeasureId = gridmeasureId; }

    public boolean isDelete() { return delete; }

    public void setDelete(boolean delete) { this.delete = delete; }

    public PowerSystemResource getPowerSystemResource() {
        return powerSystemResource;
    }

    public void setPowerSystemResource(PowerSystemResource powerSystemResource) {
        this.powerSystemResource = powerSystemResource;
    }

    public List<Steps> getListSteps() {
        return listSteps;
    }

    public void setListSteps(List<Steps> listSteps) {
        this.listSteps = listSteps;
    }
}

