/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;

public class Calender {

    @JsonProperty
    private Integer gridMeasureId;

    @JsonProperty
    private String gridMeasureTitle;

    @JsonProperty
    private Integer gridMeasureStatusId;

    @JsonProperty
    private Integer singleGridMeasureId;

    @JsonProperty
    private String singleGridMeasureTitle;

    @JsonProperty
    private Date plannedStarttimSinglemeasure;

    @JsonProperty
    private Date plannedEndtimeSinglemeasure;


    public Integer getGridMeasureId() { return gridMeasureId; }

    public void setGridMeasureId(Integer gridMeasureId) { this.gridMeasureId = gridMeasureId; }

    public Integer getSingleGridMeasureId() { return singleGridMeasureId; }

    public void setSingleGridMeasureId(Integer singleGridMeasureId) { this.singleGridMeasureId = singleGridMeasureId; }

    public String getGridMeasureTitle() { return gridMeasureTitle; }

    public void setGridMeasureTitle(String gridMeasureTitle) { this.gridMeasureTitle = gridMeasureTitle; }

    public Integer getGridMeasureStatusId() { return gridMeasureStatusId; }

    public void setGridMeasureStatusId(Integer gridMeasureStatusId) { this.gridMeasureStatusId = gridMeasureStatusId; }

    public String getSingleGridMeasureTitle() { return singleGridMeasureTitle; }

    public void setSingleGridMeasureTitle(String singleGridMeasureTitle) { this.singleGridMeasureTitle = singleGridMeasureTitle; }

    public Date getPlannedStarttimSinglemeasure() { return plannedStarttimSinglemeasure; }

    public void setPlannedStarttimSinglemeasure(Date plannedStarttimSinglemeasure) { this.plannedStarttimSinglemeasure = plannedStarttimSinglemeasure; }

    public Date getPlannedEndtimeSinglemeasure() { return plannedEndtimeSinglemeasure; }

    public void setPlannedEndtimeSinglemeasure(Date plannedEndtimeSinglemeasure) { this.plannedEndtimeSinglemeasure = plannedEndtimeSinglemeasure; }
}
