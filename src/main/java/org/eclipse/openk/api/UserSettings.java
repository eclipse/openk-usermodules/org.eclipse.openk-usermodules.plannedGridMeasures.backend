/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelClass;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelField;
import org.eclipse.openk.db.model.TblUserSettings;

@MapDbModelClass(classType = TblUserSettings.class)
public class UserSettings {
    @JsonProperty
    @MapDbModelField
    private String username;

    @JsonProperty
    @MapDbModelField
    private String settingType;

    @JsonProperty
    @MapDbModelField
    private String value;

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getSettingType() { return settingType; }

    public void setSettingType(String settingType) { this.settingType = settingType; }

    public String getValue() { return value; }

    public void setValue(String value) { this.value = value; }
}
