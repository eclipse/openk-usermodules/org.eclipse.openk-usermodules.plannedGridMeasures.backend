/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.log4j.Logger;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.openk.api.BackendSettings;
import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.eclipse.openk.core.controller.BackendConfig;
import org.eclipse.openk.core.controller.InitMailAddressCacheJob;
import org.eclipse.openk.core.controller.InitRabbitMqSetup;
import org.eclipse.openk.db.dao.EntityHelper;
import org.eclipse.openk.health.DBIsPresentHealthCheck;
import org.eclipse.openk.health.MailConfigurationHealthCheck;
import org.eclipse.openk.health.MailServerPresentHealthCheck;
import org.eclipse.openk.resources.MasterDataResource;
import org.eclipse.openk.resources.PlannedGridMeasuresResource;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import static org.eclipse.openk.common.JsonGeneratorBase.getGson;

public class PlannedGridMeasuresApplication extends Application<PlannedGridMeasuresConfiguration> {
    private static final Logger logger = Logger.getLogger(PlannedGridMeasuresApplication.class); // NOSONAR
    // we leave this logger in place, even if it's not called ... (at the moment)

    public static void main(final String[] args) throws Exception {

        new PlannedGridMeasuresApplication().run(args);
    }

    @Override
    public String getName() {
        return "PlannedGridMeasures";
    }

    @Override
    public void initialize(final Bootstrap<PlannedGridMeasuresConfiguration> bootstrap) {
        //Initializations are mainly made in the method configureApp(configuration)
    }

    public void run(final PlannedGridMeasuresConfiguration configuration,
                    final Environment environment) {

        configureApp( configuration );

        final PlannedGridMeasuresResource resource = new PlannedGridMeasuresResource();
        final MasterDataResource mdResource = new MasterDataResource();
        final DBIsPresentHealthCheck dBIsPresentHealthCheck = new DBIsPresentHealthCheck();
        final MailServerPresentHealthCheck mailServerPresentHealthCheck = new MailServerPresentHealthCheck();
        final MailConfigurationHealthCheck mailConfigurationHealthCheck = new MailConfigurationHealthCheck();


        environment.healthChecks().register("version-main", dBIsPresentHealthCheck);
        environment.healthChecks().register("mailserver-running", mailServerPresentHealthCheck);
        environment.healthChecks().register("mailconfiguration-ok", mailConfigurationHealthCheck);
        environment.jersey().register(resource);
        environment.jersey().register(mdResource);
        environment.jersey().register(MultiPartFeature.class);
        configureCors(environment);

    }

    private void configureApp( final PlannedGridMeasuresConfiguration configuration ) {

        // first set db
        EntityHelper.setFactoryName(configuration.getPersistencyUnit());
        EntityHelper.setProperties(extractDbProperties( configuration.getDbConn() ));
        BackendConfig.configure(configuration, getBackendSettings());
        BackendConfig.getInstance().processAndSetWhiteListDocumenttypes(configuration.getWhiteListDocumenttypes());
        BackendConfig.getInstance().setEmailConfiguration(configuration.getEmailConfiguration());

        InitMailAddressCacheJob initMailAddressCacheJob = new InitMailAddressCacheJob();
        initMailAddressCacheJob.init();

        InitRabbitMqSetup initRabbitMqSetup = new InitRabbitMqSetup();
        initRabbitMqSetup.init();
    }

    private BackendSettings getBackendSettings() {
        ResourceLoaderBase loaderBase = new ResourceLoaderBase();
        String backendSettingsString = loaderBase.loadFromPath("./backendSettings.json");
        return getGson().fromJson(backendSettingsString, BackendSettings.class);
    }

    private Map<String, Object> extractDbProperties( PlannedGridMeasuresConfiguration.DBConnection dbconn ) {
        Map<String, Object> retMap = new HashMap<>();
        retMap.put("javax.persistence.jdbc.driver", dbconn.getDriver());
        retMap.put("javax.persistence.jdbc.url", dbconn.getUrl());
        retMap.put("javax.persistence.jdbc.user", dbconn.getUser());
        retMap.put("javax.persistence.jdbc.password", dbconn.getPassword());
        return retMap;
    }

    private void configureCors(Environment environment) {
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Authorization");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        // DO NOT pass a preflight request to down-stream auth filters
        // unauthenticated preflight requests should be permitted by spec
        cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, Boolean.FALSE.toString());
    }


}
