/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class StepsTest {

    @Test
    public void testGetSortorder() throws IOException {
        Steps stp = new Steps();
        stp.setSortorder(4);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(stp);

        Steps stp2 = om.readValue(jsonString, Steps.class);

        assertEquals(stp.getSortorder(), stp2.getSortorder());
    }

    @Test
    public void testTargetState() throws IOException {
        Steps stp = new Steps();
        stp.setTargetState("testtarget");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(stp);

        Steps stp2 = om.readValue(jsonString, Steps.class);

        assertEquals(stp.getTargetState(), stp2.getTargetState());
    }

    @Test
    public void testSwitchingObject() throws IOException {
        Steps stp = new Steps();
        stp.setSwitchingObject("Switching Object");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(stp);

        Steps stp2 = om.readValue(jsonString, Steps.class);

        assertEquals(stp.getSwitchingObject(), stp2.getSwitchingObject());
    }

    @Test
    public void testId() throws IOException {
        Steps stp = new Steps();
        stp.setId(2);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(stp);

        Steps stp2 = om.readValue(jsonString, Steps.class);

        assertEquals(stp.getId(), stp2.getId());
    }

    @Test
    public void testSingleGridmeasureId() throws IOException {
        Steps stp = new Steps();
        stp.setSingleGridmeasureId(6);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(stp);

        Steps stp2 = om.readValue(jsonString, Steps.class);

        assertEquals(stp.getSingleGridmeasureId(), stp2.getSingleGridmeasureId());
    }

}



