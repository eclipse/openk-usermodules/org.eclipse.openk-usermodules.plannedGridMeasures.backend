/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class GmStatusTest {

    @Test
    public void testGetId()throws IOException {
        GmStatus status = new GmStatus();
        status.setId(2);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(status);

        GmStatus status2 = om.readValue(jsonString, GmStatus.class);

        assertEquals(status.getName(), status2.getName());
    }

    @Test
    public void testGetName()throws IOException {
        GmStatus status = new GmStatus();
        status.setName("Statusname");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(status);

        GmStatus status2 = om.readValue(jsonString, GmStatus.class);

        assertEquals(status.getName(), status2.getName());
    }

    @Test
    public void testGetColorCode()throws IOException {
        GmStatus status = new GmStatus();
        status.setColorCode("#222222");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(status);

        GmStatus status2 = om.readValue(jsonString, GmStatus.class);

        assertEquals(status.getColorCode(), status2.getColorCode());
    }

}
