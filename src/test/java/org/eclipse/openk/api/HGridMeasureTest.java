/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class HGridMeasureTest {

    @Test
    public void testGetHId()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setHId(4);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getDescriptiveId(), hgm2.getDescriptiveId());
    }

    @Test
    public void testGetHAction()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setHAction(4);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getDescriptiveId(), hgm2.getDescriptiveId());
    }

    @Test
    public void testGetHdate()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        Date hdate = new Date(System.currentTimeMillis());
        hgm.setHDate(hdate);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getDescriptiveId(), hgm2.getDescriptiveId());
    }

    @Test
    public void testGetHUser()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setHUser("user");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getDescriptiveId(), hgm2.getDescriptiveId());
    }

    @Test
    public void testGetDescriptiveId()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setDescriptiveId("4");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getDescriptiveId(), hgm2.getDescriptiveId());
    }

    @Test
    public void testGetTitle()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setTitle("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getTitle(), hgm2.getTitle());
    }

    @Test
    public void testGetAffected_resource()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setAffectedResource("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getAffectedResource(), hgm2.getAffectedResource());
    }

    @Test
    public void testGetRemark()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setAffectedResource("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getRemark(), hgm2.getRemark());
    }

    @Test
    public void testGetStatusId()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setStatusId(3);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getStatusId(), hgm2.getStatusId());
    }

    @Test
    public void testGetSwitchingObject()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setSwitchingObject("Transformator");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getSwitchingObject(), hgm2.getSwitchingObject());
    }

    @Test
    public void testGetCostCenter()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setCostCenter("Centre di costa");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getCostCenter(), hgm2.getCostCenter());
    }

    @Test
    public void testGetApprovalBy()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setApprovalBy("Adam Approver");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getApprovalBy(), hgm2.getApprovalBy());
    }

    @Test
    public void testGetAreaOfSwitching()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setAreaOfSwitching("Southeast");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getAreaOfSwitching(), hgm2.getAreaOfSwitching());
    }

    @Test
    public void testGetAppointmentRepetition()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setAppointmentRepetition("daily");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getAppointmentRepetition(), hgm2.getAppointmentRepetition());
    }

    @Test
    public void testGetAppointmentStartdate()throws IOException {
        HGridMeasure hgm = new HGridMeasure();

        Date dateAppointmentStartdate = new Date(System.currentTimeMillis());
        hgm.setAppointmentStartdate(dateAppointmentStartdate);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getAppointmentStartdate(), hgm2.getAppointmentStartdate());
    }

    @Test
    public void testGetAppointmentNumberOf()throws IOException {
        HGridMeasure hgm = new HGridMeasure();

        hgm.setAppointmentNumberOf(5);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getAppointmentNumberOf(), hgm2.getAppointmentNumberOf());
    }



    @Test
    public void testGetPlannedStarttimeFirstSinglemeasure()throws IOException {
        HGridMeasure hgm = new HGridMeasure();

        Date datePlannedStarttimeFirstSinglemeasure = new Date(System.currentTimeMillis());
        hgm.setPlannedStarttimeFirstSinglemeasure(datePlannedStarttimeFirstSinglemeasure);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getPlannedStarttimeFirstSinglemeasure(), hgm2.getPlannedStarttimeFirstSinglemeasure());
    }
    

    @Test
    public void testGetTimeOfReallocation()throws IOException {
        HGridMeasure hgm = new HGridMeasure();

        String dateTimeOfReallocation = "2018-04-19T08:00:00";
        hgm.setTimeOfReallocation(dateTimeOfReallocation);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getTimeOfReallocation(), hgm2.getTimeOfReallocation());
    }

    @Test
    public void testGetDescription()throws IOException {
        HGridMeasure hgm = new HGridMeasure();

        hgm.setDescription("This is a description");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getDescription(), hgm2.getDescription());
    }


    @Test
    public void testGetBranch()throws IOException {
        HGridMeasure hgm = new HGridMeasure();

        hgm.setBranchId(2);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getBranchId(), hgm2.getBranchId());
    }

    @Test
    public void testGetLevel()throws IOException {
        HGridMeasure hgm = new HGridMeasure();

        hgm.setBranchLevelId(3);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getBranchLevelId(), hgm2.getBranchLevelId());
    }

    @Test
    public void testGetCreate_user_department()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setAffectedResource("Test");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getCreateUserDepartment(), hgm2.getCreateUserDepartment());
    }

    @Test
    public void testGetModUserDepartment()throws IOException {
        HGridMeasure hgm = new HGridMeasure();
        hgm.setModUserDepartment("Department YY");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(hgm);

        HGridMeasure hgm2 = om.readValue(jsonString, HGridMeasure.class);

        assertEquals(hgm.getModUserDepartment(), hgm2.getModUserDepartment());
    }

}






