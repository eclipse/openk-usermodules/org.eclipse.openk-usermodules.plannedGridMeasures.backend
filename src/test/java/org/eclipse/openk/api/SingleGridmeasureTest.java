/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;
import org.junit.Test;

public class SingleGridmeasureTest {

    @Test
    public void testGetSortorder() throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();
        sgm.setSortorder(4);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getSortorder(), sgm2.getSortorder());
    }

    @Test
    public void testTitle() throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();
        sgm.setTitle("Testtitle");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getTitle(), sgm2.getTitle());
    }

    @Test
    public void testSwitchingObject() throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();
        sgm.setSwitchingObject("Switching Object");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getSwitchingObject(), sgm2.getSwitchingObject());
    }

    @Test
    public void testPowerSystemResource() throws IOException {
        PowerSystemResource psr = new PowerSystemResource();
        psr.setCimId("4711");
        psr.setCimName("CIM");
        psr.setCimDescription("CIMDesc");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(psr);

        PowerSystemResource psr2 = om.readValue(jsonString, PowerSystemResource.class);

        assertEquals(psr.getCimId(), psr2.getCimId());
        assertEquals(psr.getCimName(), psr2.getCimName());
        assertEquals(psr.getCimDescription(), psr2.getCimDescription());
    }

    @Test
    public void testPlannedStarttimeSinglemeasure() throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();

        java.util.Date plannedStarttimeSinglemeasure = new Date(System.currentTimeMillis());
        sgm.setPlannedStarttimeSinglemeasure(plannedStarttimeSinglemeasure);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getPlannedStarttimeSinglemeasure(), sgm2.getPlannedStarttimeSinglemeasure());
    }

    @Test
    public void testPlannedEndtimeSinglemeasure() throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();

        java.util.Date plannedEndtimeSinglemeasure = new Date(System.currentTimeMillis());
        sgm.setPlannedEndtimeSinglemeasure(plannedEndtimeSinglemeasure);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getPlannedEndtimeSinglemeasure(), sgm2.getPlannedEndtimeSinglemeasure());
    }

    @Test
    public void testDescription() throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();
        sgm.setDescription("Description");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getDescription(), sgm2.getDescription());
    }

    @Test
    public void testGetResponsibleOnSiteName()throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();
        sgm.setResponsibleOnSiteName("Rudi Responsible");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getResponsibleOnSiteName(), sgm2.getResponsibleOnSiteName());
    }

    @Test
    public void testGetResponsibleOnSiteDepartment()throws IOException {
        SingleGridmeasure sgm = new SingleGridmeasure();
        sgm.setResponsibleOnSiteDepartment("Department YY");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(sgm);

        SingleGridmeasure sgm2 = om.readValue(jsonString, SingleGridmeasure.class);

        assertEquals(sgm.getResponsibleOnSiteDepartment(), sgm2.getResponsibleOnSiteDepartment());
    }

}



