/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class CostCenterTest {

    @Test
    public void testGetId()throws IOException {
        CostCenter costCenter = new CostCenter();
        costCenter.setId(7);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(costCenter);

        CostCenter costCenter2 = om.readValue(jsonString, CostCenter.class);

        assertEquals(costCenter.getId(), costCenter2.getId());
    }

    @Test
    public void testGetName()throws IOException {
        CostCenter costCenter = new CostCenter();
        costCenter.setName("CostCenterName");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(costCenter);

        CostCenter costCenter2 = om.readValue(jsonString, CostCenter.class);

        assertEquals(costCenter.getName(), costCenter2.getName());
    }

    @Test
    public void testGetDescription()throws IOException {
        CostCenter costCenter = new CostCenter();
        costCenter.setDescription("Cost Center Description");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(costCenter);

        CostCenter costCenter2 = om.readValue(jsonString, CostCenter.class);

        assertEquals(costCenter.getDescription(), costCenter2.getDescription());
    }
}
