/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class VersionInfoTest {

    @Test
    public void testGetVersionInfo() throws IOException {
        VersionInfo vi = new VersionInfo();
        vi.setBackendVersion("123");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(vi);

        VersionInfo vi2 = om.readValue(jsonString, VersionInfo.class);

        assertEquals(vi.getBackendVersion(), vi2.getBackendVersion());
    }
}
