/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class FilterObjectTest {

    @Test
    public void testIsClosedStatusActive()throws IOException {
        FilterObject fo = new FilterObject();
        fo.setClosedStatusActive(true);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(fo);

        FilterObject fo2 = om.readValue(jsonString, FilterObject.class);

        assertEquals(fo.isClosedStatusActive(), fo2.isClosedStatusActive());
    }

    @Test
    public void testIsCancelStatusActive()throws IOException {
        FilterObject fo = new FilterObject();
        fo.setCanceledStatusActive(true);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(fo);

        FilterObject fo2 = om.readValue(jsonString, FilterObject.class);

        assertEquals(fo.isCanceledStatusActive(), fo2.isCanceledStatusActive());
    }
}
