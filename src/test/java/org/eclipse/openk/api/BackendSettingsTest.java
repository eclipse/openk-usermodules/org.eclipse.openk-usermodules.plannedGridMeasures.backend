/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.junit.Test;

import java.io.IOException;

import static org.eclipse.openk.common.JsonGeneratorBase.getGson;
import static org.junit.Assert.assertEquals;

public class BackendSettingsTest {

    @Test
    public void testGetReminderPeriod()throws IOException {
        BackendSettings backendSettings = new BackendSettings();
        backendSettings.setReminderPeriod(48);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings backendSettings2 = om.readValue(jsonString, BackendSettings.class);

        assertEquals(backendSettings.getReminderPeriod(), backendSettings2.getReminderPeriod());
    }

    @Test
    public void testIsSkipForApproval()throws IOException {
        BackendSettings.BpmnGridConfig backendSettings = new BackendSettings.BpmnGridConfig();
        backendSettings.setSkipForApproval(true);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings.BpmnGridConfig backendSettings2 = om.readValue(jsonString, BackendSettings.BpmnGridConfig.class);

        assertEquals(backendSettings.isSkipForApproval(), backendSettings2.isSkipForApproval());
    }

    @Test
    public void testEndAfterAppoved()throws IOException {
        BackendSettings.BpmnGridConfig backendSettings = new BackendSettings.BpmnGridConfig();
        backendSettings.setEndAfterApproved(true);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings.BpmnGridConfig backendSettings2 = om.readValue(jsonString, BackendSettings.BpmnGridConfig.class);

        assertEquals(backendSettings.isEndAfterApproved(), backendSettings2.isEndAfterApproved());
    }

    @Test
    public void testSkipRequesting()throws IOException {
        BackendSettings.BpmnGridConfig backendSettings = new BackendSettings.BpmnGridConfig();
        backendSettings.setSkipRequesting(true);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings.BpmnGridConfig backendSettings2 = om.readValue(jsonString, BackendSettings.BpmnGridConfig.class);

        assertEquals(backendSettings.isSkipRequesting(), backendSettings2.isSkipRequesting());
    }

    @Test
    public void testEndAfterReleased()throws IOException {
        BackendSettings.BpmnGridConfig backendSettings = new BackendSettings.BpmnGridConfig();
        backendSettings.setEndAfterReleased(true);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings.BpmnGridConfig backendSettings2 = om.readValue(jsonString, BackendSettings.BpmnGridConfig.class);

        assertEquals(backendSettings.isEndAfterReleased(), backendSettings2.isEndAfterReleased());
    }

    @Test
    public void testSkipInWork()throws IOException {
        BackendSettings.BpmnGridConfig backendSettings = new BackendSettings.BpmnGridConfig();
        backendSettings.setSkipInWork(true);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings.BpmnGridConfig backendSettings2 = om.readValue(jsonString, BackendSettings.BpmnGridConfig.class);

        assertEquals(backendSettings.isSkipInWork(), backendSettings2.isSkipInWork());
    }

    ///////////////////////////


    @Test
    public void testGetIdForAppointmentRepetition()throws IOException {
        BackendSettings.AppointmentRepetition backendSettings = new BackendSettings.AppointmentRepetition();
        backendSettings.setId(1);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings.AppointmentRepetition backendSettings2 = om.readValue(jsonString, BackendSettings.AppointmentRepetition.class);

        assertEquals(backendSettings.getId(), backendSettings2.getId());
    }

    @Test
    public void testGetNameForAppointmentRepetition()throws IOException {
        BackendSettings.AppointmentRepetition backendSettings = new BackendSettings.AppointmentRepetition();
        backendSettings.setName("täglich");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(backendSettings);

        BackendSettings.AppointmentRepetition backendSettings2 = om.readValue(jsonString, BackendSettings.AppointmentRepetition.class);

        assertEquals(backendSettings.getName(), backendSettings2.getName());
    }


    ////////////////////////




    public static BackendSettings getBackendSettings() {
        ResourceLoaderBase loaderBase = new ResourceLoaderBase();
        String backendSettingsString = loaderBase.loadFromPath("./backendSettings.json");
        return getGson().fromJson(backendSettingsString, BackendSettings.class);
    }
}
