/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.api.mail;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class EmailTemplatePathsTest {

    @Test
    public void testGetAppliedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setAppliedEmailTemplate("Applied Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getAppliedEmailTemplate(), template2.getAppliedEmailTemplate());
    }

    @Test
    public void testGetForapprovalEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setForapprovalEmailTemplate("For Approval Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getForapprovalEmailTemplate(), template2.getForapprovalEmailTemplate());
    }

    @Test
    public void testGetApprovedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setApprovedEmailTemplate("Approved Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getApprovedEmailTemplate(), template2.getApprovedEmailTemplate());
    }

    @Test
    public void testGetRequestedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setRequestedEmailTemplate("Requested Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getRequestedEmailTemplate(), template2.getRequestedEmailTemplate());
    }

    @Test
    public void testGetReleasedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setReleasedEmailTemplate("Released Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getReleasedEmailTemplate(), template2.getReleasedEmailTemplate());
    }

    @Test
    public void testGetActiveEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setActiveEmailTemplate("Active Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getActiveEmailTemplate(), template2.getActiveEmailTemplate());
    }

    @Test
    public void testGetInworkEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setInworkEmailTemplate("In work Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getInworkEmailTemplate(), template2.getInworkEmailTemplate());
    }

    @Test
    public void testGetWorkfinishedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setWorkfinishedEmailTemplate("Work finished Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getWorkfinishedEmailTemplate(), template2.getWorkfinishedEmailTemplate());
    }

    @Test
    public void testGetFinishedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setFinishedEmailTemplate("Finished Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getFinishedEmailTemplate(), template2.getFinishedEmailTemplate());
    }

    @Test
    public void testGetClosedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setClosedEmailTemplate("Closed Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getClosedEmailTemplate(), template2.getClosedEmailTemplate());
    }

    @Test
    public void testGetCancelledEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setCancelledEmailTemplate("Canceled Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getCancelledEmailTemplate(), template2.getCancelledEmailTemplate());
    }

    @Test
    public void testGetRejectedEmailTemplate()throws IOException {
        EmailTemplatePaths template = new EmailTemplatePaths();
        template.setRejectedEmailTemplate("Rejected Template Text");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(template);

        EmailTemplatePaths template2 = om.readValue(jsonString, EmailTemplatePaths.class);

        assertEquals(template.getRejectedEmailTemplate(), template2.getRejectedEmailTemplate());
    }
}
