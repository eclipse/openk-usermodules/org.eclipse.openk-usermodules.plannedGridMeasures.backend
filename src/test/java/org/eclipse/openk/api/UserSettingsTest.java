/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class UserSettingsTest {
    @Test
    public void testGetUserame()throws IOException {
        UserSettings userSettings = new UserSettings();
        userSettings.setUsername("Username");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(userSettings);

        UserSettings userSettings2 = om.readValue(jsonString, UserSettings.class);

        assertEquals(userSettings.getUsername(), userSettings2.getUsername());
    }

    @Test
    public void testGetSettingType()throws IOException {
        UserSettings userSettings = new UserSettings();
        userSettings.setSettingType("SettingType");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(userSettings);

        UserSettings userSettings2 = om.readValue(jsonString, UserSettings.class);

        assertEquals(userSettings.getSettingType(), userSettings2.getSettingType());
    }

    @Test
    public void testGetValue()throws IOException {
        UserSettings userSettings = new UserSettings();
        userSettings.setValue("{\"menu\": {\n" +
                "  \"id\": \"file\",\n" +
                "  \"value\": \"File\",\n" +
                "  \"popup\": {\n" +
                "    \"menuitem\": [\n" +
                "      {\"value\": \"New\", \"onclick\": \"CreateNewDoc()\"},\n" +
                "      {\"value\": \"Open\", \"onclick\": \"OpenDoc()\"},\n" +
                "      {\"value\": \"Close\", \"onclick\": \"CloseDoc()\"}\n" +
                "    ]\n" +
                "  }\n" +
                "}}");
        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(userSettings);

        UserSettings userSettings2 = om.readValue(jsonString, UserSettings.class);

        assertEquals(userSettings.getValue(), userSettings2.getValue());
    }
}
