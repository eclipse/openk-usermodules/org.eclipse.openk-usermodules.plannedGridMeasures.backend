/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Date;

import org.junit.Test;

public class CalenderTest {

    @Test
    public void testGetGridMeasureId()throws IOException {
        Calender calender = new Calender();
        calender.setGridMeasureId(5);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(calender);

        Calender calender2 = om.readValue(jsonString, Calender.class);

        assertEquals(calender.getGridMeasureId(), calender2.getGridMeasureId());
    }

    @Test
    public void testGetGridMeasureTitle()throws IOException {
        Calender calender = new Calender();
        calender.setGridMeasureTitle("Test 5");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(calender);

        Calender calender2 = om.readValue(jsonString, Calender.class);

        assertEquals(calender.getGridMeasureTitle(), calender2.getGridMeasureTitle());
    }

    @Test
    public void testGetGridMeasureStatusId()throws IOException {
        Calender calender = new Calender();
        calender.setGridMeasureStatusId(3);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(calender);

        Calender calender2 = om.readValue(jsonString, Calender.class);

        assertEquals(calender.getGridMeasureStatusId(), calender2.getGridMeasureStatusId());
    }

    @Test
    public void testGetSingleGridMeasureId()throws IOException {
        Calender calender = new Calender();
        calender.setSingleGridMeasureId(2);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(calender);

        Calender calender2 = om.readValue(jsonString, Calender.class);

        assertEquals(calender.getSingleGridMeasureId(), calender2.getSingleGridMeasureId());
    }

    @Test
    public void testGetSingleGridMeasureTitle()throws IOException {
        Calender calender = new Calender();
        calender.setSingleGridMeasureTitle("Test Single 2");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(calender);

        Calender calender2 = om.readValue(jsonString, Calender.class);

        assertEquals(calender.getSingleGridMeasureTitle(), calender2.getSingleGridMeasureTitle());
    }

    @Test
    public void testGetPlannedStarttimSinglemeasure()throws IOException {
        Calender calender = new Calender();

        java.util.Date plannedStarttimeSinglemeasure = new Date(System.currentTimeMillis());
        calender.setPlannedStarttimSinglemeasure(plannedStarttimeSinglemeasure);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(calender);

        Calender calender2 = om.readValue(jsonString, Calender.class);

        assertEquals(calender.getPlannedStarttimSinglemeasure(), calender2.getPlannedStarttimSinglemeasure());
    }

    @Test
    public void testGetPlannedEndtimeSinglemeasure()throws IOException {
        Calender calender = new Calender();

        java.util.Date plannedEndtimeSinglemeasure = new Date(System.currentTimeMillis());
        calender.setPlannedEndtimeSinglemeasure(plannedEndtimeSinglemeasure);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(calender);

        Calender calender2 = om.readValue(jsonString, Calender.class);

        assertEquals(calender.getPlannedEndtimeSinglemeasure(), calender2.getPlannedEndtimeSinglemeasure());
    }


}

