/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Test;

public class TerritoryTest {

    @Test
    public void testGetId()throws IOException {
        Territory territory = new Territory();
        territory.setId(6);

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(territory);

        Territory territory2 = om.readValue(jsonString, Territory.class);

        assertEquals(territory.getId(), territory2.getId());
    }

    @Test
    public void testGetName()throws IOException {
        Territory territory = new Territory();
        territory.setName("territoryName");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(territory);

        Territory territory2 = om.readValue(jsonString, Territory.class);

        assertEquals(territory.getName(), territory2.getName());
    }

    @Test
    public void testGetDescription()throws IOException {
        Territory territory = new Territory();
        territory.setDescription("Territory Description");

        ObjectMapper om = new ObjectMapper();
        String jsonString = om.writeValueAsString(territory);

        Territory territory2 = om.readValue(jsonString, Territory.class);

        assertEquals(territory.getDescription(), territory2.getDescription());
    }
}

