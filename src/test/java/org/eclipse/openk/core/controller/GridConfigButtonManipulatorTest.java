/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.BackendSettings;
import org.eclipse.openk.common.util.ResourceLoaderBase;
import org.junit.Before;
import org.junit.Test;

import static org.eclipse.openk.common.JsonGeneratorBase.getGson;
import static org.junit.Assert.assertEquals;

public class GridConfigButtonManipulatorTest {

    @Before
    public void initSettings() {
        TestHelper.initDefaultBackendConfig();
    }

    @Test
    public void testManipulateButtons1() {
        RoleAccessDefinitions.INSTANCE.load();
        BackendSettings.BpmnGridConfig gridConfig = getGridConfig(false,
                false,
                false,
                false,
                false);
        RoleAccessDefinitions.INSTANCE.setDirty();
        RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(x -> new GridConfigButtonManipulator(gridConfig).manipulateButtons(x ));

        assertEquals("forapproval", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[1].getActiveButtons()[1]);
        assertEquals("request", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[4].getActiveButtons()[2]);
        assertEquals("activate", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[6].getActiveButtons()[1]);
        assertEquals("inwork", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[7].getActiveButtons()[1]);


    }


    @Test
    public void testManipulateButtons2() {
        BackendSettings.BpmnGridConfig gridConfig = getGridConfig(true,
                false,
                false,
                false,
                false);
        RoleAccessDefinitions.INSTANCE.setDirty();
        RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(x -> new GridConfigButtonManipulator(gridConfig).manipulateButtons(x ));
        assertEquals("approve", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[1].getActiveButtons()[1]);
    }

    @Test
    public void testManipulateButtons3() {
        BackendSettings.BpmnGridConfig gridConfig = getGridConfig(true,
                true,
                false,
                false,
                false);
        RoleAccessDefinitions.INSTANCE.setDirty();
        RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(x -> new GridConfigButtonManipulator(gridConfig).manipulateButtons(x ));

        assertEquals("close", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[4].getActiveButtons()[2]);
    }

    @Test
    public void testManipulateButtons4() {
        BackendSettings.BpmnGridConfig gridConfig = getGridConfig(true,
                false,
                true,
                false,
                false);

        RoleAccessDefinitions.INSTANCE.setDirty();
        RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(x -> new GridConfigButtonManipulator(gridConfig).manipulateButtons(x ));

        assertEquals("activate", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[4].getActiveButtons()[2]);
    }


    @Test
    public void testManipulateButtons5() {
        BackendSettings.BpmnGridConfig gridConfig = getGridConfig(true,
                true,
                true,
                false,
                false);

        RoleAccessDefinitions.INSTANCE.setDirty();
        RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(x -> new GridConfigButtonManipulator(gridConfig).manipulateButtons(x ));

        assertEquals("close", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[4].getActiveButtons()[2]);

    }

    @Test
    public void testManipulateButtons6() {
        BackendSettings.BpmnGridConfig gridConfig = getGridConfig(true,
                true,
                true,
                true,
                false);

        RoleAccessDefinitions.INSTANCE.setDirty();
        RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(x -> new GridConfigButtonManipulator(gridConfig).manipulateButtons(x ));

        assertEquals("close", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[6].getActiveButtons()[1]);
    }


    @Test
    public void testManipulateButtons7() {
        BackendSettings.BpmnGridConfig gridConfig = getGridConfig(true,
                true,
                true,
                true,
                true);

        RoleAccessDefinitions.INSTANCE.setDirty();
        RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(x -> new GridConfigButtonManipulator(gridConfig).manipulateButtons(x ));

        assertEquals("finish", RoleAccessDefinitions.INSTANCE.getRoleAccessDefinition(null).getControls()[7].getActiveButtons()[1]);
    }







    private BackendSettings.BpmnGridConfig getGridConfig(boolean skipForApproval,
                                                         boolean endAfterApproved,
                                                         boolean skipRequesting,
                                                         boolean endAfterReleased,
                                                         boolean skipInWork) {
        BackendSettings.BpmnGridConfig config = new BackendSettings.BpmnGridConfig();
        config.setSkipForApproval(skipForApproval);
        config.setEndAfterApproved(endAfterApproved);
        config.setSkipRequesting(skipRequesting);
        config.setEndAfterReleased(endAfterReleased);
        config.setSkipInWork(skipInWork);
        return config;
    }

    private BackendSettings getBackendSettings() {
        ResourceLoaderBase loaderBase = new ResourceLoaderBase();
        String backendSettingsString = loaderBase.loadFromPath("./backendSettings.json");
        return getGson().fromJson(backendSettingsString, BackendSettings.class);
    }
}
