/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.controller;

import org.easymock.EasyMock;
import org.eclipse.openk.db.dao.*;
import org.eclipse.openk.db.model.RefBranch;
import org.eclipse.openk.db.model.TblGridMeasure;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertTrue;

public class MailAddressCacheTimerTaskTest {

    private class TestableMailAddressCacheTimerTask extends MailAddressCacheTimerTask {
        protected TblGridMeasureDao tblGridMeasureDao;

        @Override
        protected AutoCloseEntityManager createEm() {
            EntityTransaction et = EasyMock.createMock(EntityTransaction.class);
            et.begin();
            expectLastCall().andVoid().anyTimes();
            et.commit();
            expectLastCall().andVoid().anyTimes();
            et.rollback();
            expectLastCall().andVoid().anyTimes();

            EasyMock.replay(et);
            EasyMock.verify(et);

            EntityManager em = EasyMock.createMock(EntityManager.class);
            expect(em.getTransaction()).andReturn(et).anyTimes();

            expect(em.isOpen()).andReturn(false).anyTimes();

            em.close();
            expectLastCall().andVoid().anyTimes();

            EasyMock.replay(em);
            EasyMock.verify(em);

            return new AutoCloseEntityManager(em);
        }

        protected TblGridMeasureDao createTblGridMeasureDao(EntityManager em) {
            if( tblGridMeasureDao != null ) {
                return tblGridMeasureDao;
            }
            else {
                return super.createTblGridMeasureDao(em);
            }
        }




    }

    private TblGridMeasureDao createTblGridMeasureDao() {

        java.util.List<String> mList = new ArrayList<String>();

        TblGridMeasure measure1 = new TblGridMeasure();
        measure1.setId(1);
        measure1.setEmailAddresses("Testmail1@test.de");
        mList.add("Testmail1@test.de");

        TblGridMeasure measure2 = new TblGridMeasure();
        measure1.setId(2);
        measure1.setEmailAddresses("Testmail2@test.de");
        mList.add("Testmail2@test.de");


        TblGridMeasureDao dao = EasyMock.createMock(TblGridMeasureDao.class);
        expect( dao.getMailAddressesFromGridmeasures() ).andReturn(mList).anyTimes();
        replay( dao );
        verify( dao );
        return dao;
    }

    @Test
    public void testRun() {

        TblGridMeasureDao dao = createTblGridMeasureDao();
        MailAddressCacheTimerTaskTest.TestableMailAddressCacheTimerTask ttt = new MailAddressCacheTimerTaskTest.TestableMailAddressCacheTimerTask();
        ttt.tblGridMeasureDao = createTblGridMeasureDao();
        ttt.run();
        List<String> mailAddressList = MailAddressCache.getInstance().getMailAddresses();

        assertTrue(mailAddressList.size() == 2);
    }
}
