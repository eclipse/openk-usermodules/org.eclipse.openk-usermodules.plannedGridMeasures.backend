/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.plgm;

import static junit.framework.TestCase.assertEquals;

import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.junit.Test;

public class PlgmProcessSubjectTest {
    @Test (expected = RuntimeException.class )
    public void testConstructor()  {
        GridMeasure gm = new GridMeasure();
        gm.setStatusId(666);
        PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(null, "moduser");
        assertEquals(new Integer(666), subject.getGridMeasure().getStatusId()  );
        assertEquals("moduser", subject.getChangeUser());


    }
}
