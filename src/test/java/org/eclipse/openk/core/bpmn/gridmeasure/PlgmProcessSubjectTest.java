/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure;

import static junit.framework.TestCase.assertEquals;

import org.eclipse.openk.api.GridMeasure;
import org.junit.Test;


public class PlgmProcessSubjectTest {

    @Test
    public void testAll() {
        GridMeasure gm = new GridMeasure();
        gm.setStatusId(45);

        PlgmProcessSubject sub = PlgmProcessSubject.fromGridMeasure(gm, "fd");
        assertEquals( 45, sub.getGridMeasure().getStatusId().intValue() );
        GridMeasure gm2 = new GridMeasure();
        gm2.setStatusId(67);
        sub.setGridMeasure(gm2);
        sub.setChangeUser("Meyster");

        assertEquals( 67, sub.getGridMeasure().getStatusId().intValue());
        assertEquals( "Meyster", sub.getChangeUser());
    }
}
