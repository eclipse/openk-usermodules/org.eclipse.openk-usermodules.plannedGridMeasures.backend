/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.plgm;

import static junit.framework.TestCase.assertTrue;

import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.ProcessGridImpl;
import org.eclipse.openk.core.bpmn.base.ProcessStateImpl;
import org.eclipse.openk.core.bpmn.base.ServiceTaskImpl;
import org.eclipse.openk.core.bpmn.base.TestProcessSubject;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmGrid;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;

public class ProcessGridTest {
    @Test
    public void testProcessGrid() throws ProcessException, HttpStatusException {
        ProcessGridImpl theGrid = new ProcessGridImpl();
        TestProcessSubject subject = new TestProcessSubject();
        theGrid.recover(subject).start(() -> ProcessStateImpl.UITASK);
        ServiceTaskImpl serviceTask = (ServiceTaskImpl)theGrid.getService();
        assertTrue(serviceTask.leaveStepCalled);
    }

    @Test (expected = ProcessException.class)
    public void testProcessGrid_unreachable() throws ProcessException, HttpStatusException {
        ProcessGridImpl theGrid = new ProcessGridImpl();
        TestProcessSubject subject = new TestProcessSubject();
        theGrid.recover(subject).start(()-> ProcessStateImpl.UNREACHABLE);
    }

    @Test (expected = RuntimeException.class)
    public void testErrorInInit() {
        PlgmGrid pg = new PlgmGrid() {
            protected void init() throws ProcessException {
                throw new ProcessException("");
            }
        };

    }

}
