/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/


package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.easymock.EasyMock;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.assertNotNull;
import static org.easymock.EasyMock.*;

public class MeasureForApprovalTest {

    @Test
    public void MeasureForApprovalTest() throws Exception{
        GridMeasure gm = new GridMeasure();
        PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(gm, "fd");

        MeasureForApproval  ma = new MeasureForApproval();

        assertNotNull(ma);
    }

    @Test
    public void onLeaveStepTest() throws Exception{
        GridMeasure gm = new GridMeasure();
        PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(gm, "fd");

        MeasureForApproval ma  = EasyMock.createMock( MeasureForApproval.class );
        ma.onLeaveStep(subject);
        expectLastCall().andVoid().anyTimes();

        EasyMock.replay(ma);
        EasyMock.verify(ma);
    }

    @Test
    public void onEnterStepTest() throws Exception {
        GridMeasure gm = new GridMeasure();
        PlgmProcessSubject subject = PlgmProcessSubject.fromGridMeasure(gm, "fd");

        MeasureForApproval ma  = EasyMock.createMock( MeasureForApproval.class );
        ma.onEnterStep(subject);
        expectLastCall().andVoid().anyTimes();

        EasyMock.replay(ma);
        EasyMock.verify(ma);
    }
}
