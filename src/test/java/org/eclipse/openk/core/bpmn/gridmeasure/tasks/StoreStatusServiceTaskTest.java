/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/



package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class StoreStatusServiceTaskTest {

    @Test
    public void storeStatusServiceTaskTest() throws Exception{
        PlgmProcessState st = Whitebox.invokeMethod(PlgmProcessState.class, PlgmProcessState.FOR_APPROVAL.getStatusValue());

        StoreStatusServiceTask  ma = new StoreStatusServiceTask(st);
        assertNotNull(ma);
        assertEquals(st.getStatusValue(), PlgmProcessState.FOR_APPROVAL.getStatusValue());
        assertEquals(st.name(), "FOR_APPROVAL");
    }
}
