/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.gridmeasure.tasks;

import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.base.tasks.DecisionTask;
import org.eclipse.openk.core.controller.BackendConfig;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ConfigDecisionTaskTest {


    @Before
    public void initSettings() {
        TestHelper.initDefaultBackendConfig();
    }

    @Test
    public void testDecide() throws ProcessException {
        ConfigDecisionTask cdt = new ConfigDecisionTask("skipForApproval");

        BackendConfig.getInstance().getBackendSettings().getBpmnGridConfig().setSkipForApproval(true);
        assertEquals( DecisionTask.OutputPort.YES, cdt.decide(null));

        BackendConfig.getInstance().getBackendSettings().getBpmnGridConfig().setSkipForApproval(false);
        assertEquals( DecisionTask.OutputPort.NO, cdt.decide(null));
    }

    @Test( expected = ProcessException.class )
    public void testDecideException() throws ProcessException {
        ConfigDecisionTask cdt = new ConfigDecisionTask("unknownField");
        BackendConfig.getInstance().getBackendSettings().getBpmnGridConfig().setSkipForApproval(true);

        cdt.decide(null);
    }

}
