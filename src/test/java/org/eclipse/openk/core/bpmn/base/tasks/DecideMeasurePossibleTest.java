/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.core.bpmn.base.tasks;
import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.core.bpmn.base.ProcessException;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessState;
import org.eclipse.openk.core.bpmn.gridmeasure.PlgmProcessSubject;
import org.eclipse.openk.core.bpmn.gridmeasure.tasks.DecideMeasurePossible;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import static junit.framework.TestCase.assertEquals;


public class DecideMeasurePossibleTest {

    @Test
    public void decideMethodStateForApprovalTest() throws ProcessException {
        DecideMeasurePossible dmp = new DecideMeasurePossible();
        PlgmProcessSubject pps = null;
        try {
            pps = Whitebox.invokeConstructor(PlgmProcessSubject.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        GridMeasure gm = new GridMeasure();
        gm.setStatusId( PlgmProcessState.FOR_APPROVAL.getStatusValue() );
        if (pps != null){
            pps.setGridMeasure(gm);
            DecisionTask.OutputPort r = dmp.decide(pps);
            assertEquals(DecisionTask.OutputPort.YES, r);
        }

    }

    @Test
    public void decideMethodStateCanceledTest() throws ProcessException {
        DecideMeasurePossible dmp = new DecideMeasurePossible();
        PlgmProcessSubject pps = null;
        try {
            pps = Whitebox.invokeConstructor(PlgmProcessSubject.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        GridMeasure gm = new GridMeasure();
        gm.setStatusId( PlgmProcessState.CANCELED.getStatusValue() );
        if (pps != null){
            pps.setGridMeasure(gm);
            DecisionTask.OutputPort r = dmp.decide(pps);
            assertEquals(DecisionTask.OutputPort.NO, r);
        }

    }

    @Test
    public void decideMethodStateInvalidTest() throws ProcessException {
        DecideMeasurePossible dmp = new DecideMeasurePossible();
        PlgmProcessSubject pps = null;
        try {
            pps = Whitebox.invokeConstructor(PlgmProcessSubject.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        GridMeasure gm = new GridMeasure();
        gm.setStatusId( PlgmProcessState.NEW.getStatusValue() );
        if (pps != null){
            pps.setGridMeasure(gm);
            DecisionTask.OutputPort r = dmp.decide(pps);
            assertEquals(DecisionTask.OutputPort.YES, r);
        }
    }

}
