/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import static junit.framework.TestCase.assertEquals;

import org.junit.Test;

public class TblGridMeasureTest {



    @Test
    public void testGettersAndSetters() {

        TblGridMeasure tblGridmeasure = new TblGridMeasure();

        tblGridmeasure.setId(1);
        org.junit.Assert.assertEquals((Integer)1, tblGridmeasure.getId());

        tblGridmeasure.setIdDescriptive("20");
        assertEquals("20", tblGridmeasure.getIdDescriptive());

        tblGridmeasure.setTitle("Title");
        assertEquals("Title", tblGridmeasure.getTitle());

        tblGridmeasure.setAffectedResource("Affected resource");
        assertEquals("Affected resource", tblGridmeasure.getAffectedResource());

        tblGridmeasure.setRemark("Remark");
        assertEquals("Remark", tblGridmeasure.getRemark());

        tblGridmeasure.setEmailAddresses("test@email.de");
        assertEquals("test@email.de", tblGridmeasure.getEmailAddresses());

        tblGridmeasure.setFkRefGmStatus(8);
        org.junit.Assert.assertEquals((Integer)8, tblGridmeasure.getFkRefGmStatus());

        tblGridmeasure.setSwitchingObject("Transformator");
        assertEquals("Transformator", tblGridmeasure.getSwitchingObject());

        tblGridmeasure.setCostCenter("costa punta");
        assertEquals("costa punta", tblGridmeasure.getCostCenter());

        tblGridmeasure.setApprovalBy("Abel Approver");
        assertEquals("Abel Approver", tblGridmeasure.getApprovalBy());

        tblGridmeasure.setAreaOfSwitching("Northwest");
        assertEquals("Northwest", tblGridmeasure.getAreaOfSwitching());

        tblGridmeasure.setAppointmentRepetition("weekly");
        assertEquals("weekly", tblGridmeasure.getAppointmentRepetition());

        java.time.LocalDateTime ldtAppointmentStartdate = java.time.LocalDateTime.parse("2018-04-22T18:00:00");
        java.sql.Timestamp tsAppointmentStartdate = java.sql.Timestamp.valueOf(ldtAppointmentStartdate);
        tblGridmeasure.setAppointmentStartdate(tsAppointmentStartdate);
        org.junit.Assert.assertEquals(tsAppointmentStartdate, tblGridmeasure.getAppointmentStartdate());

        tblGridmeasure.setAppointmentNumberOf(5);

        java.time.LocalDateTime ldtPlannedStarttimeFirstSinglemeasure = java.time.LocalDateTime.parse("2018-04-24T20:00:00");
        java.sql.Timestamp tsPlannedStarttimeFirstSinglemeasure = java.sql.Timestamp.valueOf(ldtPlannedStarttimeFirstSinglemeasure);
        tblGridmeasure.setPlannedStarttimeFirstSinglemeasure(tsPlannedStarttimeFirstSinglemeasure);
        org.junit.Assert.assertEquals(tsPlannedStarttimeFirstSinglemeasure, tblGridmeasure.getPlannedStarttimeFirstSinglemeasure());

        java.time.LocalDateTime ldtEndtimeGridmeasure = java.time.LocalDateTime.parse("2018-04-30T03:00:00");
        java.sql.Timestamp tsEndtimeGridmeasure = java.sql.Timestamp.valueOf(ldtEndtimeGridmeasure);
        tblGridmeasure.setEndtimeGridmeasure(tsEndtimeGridmeasure);
        org.junit.Assert.assertEquals(tsEndtimeGridmeasure, tblGridmeasure.getEndtimeGridmeasure());

        String tsTimeOfReallocation = "2018-04-19T08:00:00";
        tblGridmeasure.setTimeOfReallocation(tsTimeOfReallocation);
        org.junit.Assert.assertEquals(tsTimeOfReallocation, tblGridmeasure.getTimeOfReallocation());

        tblGridmeasure.setDescription("This is a description");
        assertEquals("This is a description", tblGridmeasure.getDescription());

        tblGridmeasure.setFkRefBranch(3);
        assertEquals((Integer)3, tblGridmeasure.getFkRefBranch());

        tblGridmeasure.setFkRefBranchLevel(2);
        assertEquals((Integer)2, tblGridmeasure.getFkRefBranchLevel());

        tblGridmeasure.setCreateUser("Create User");
        assertEquals("Create User", tblGridmeasure.getCreateUser());

        tblGridmeasure.setCreateUserDepartment("Create User Department");
        assertEquals("Create User Department", tblGridmeasure.getCreateUserDepartment());

        java.time.LocalDateTime ldtCreate = java.time.LocalDateTime.parse("2018-03-20T15:00:00");
        java.sql.Timestamp tsCreate = java.sql.Timestamp.valueOf(ldtCreate);

        tblGridmeasure.setCreateDate(tsCreate);
        org.junit.Assert.assertEquals(tsCreate, tblGridmeasure.getCreateDate());

        tblGridmeasure.setModUser("Modifiing User");
        assertEquals("Modifiing User", tblGridmeasure.getModUser());

        tblGridmeasure.setModUserDepartment("Modifiing User Department");
        assertEquals("Modifiing User Department", tblGridmeasure.getModUserDepartment());

        java.time.LocalDateTime ldtMod = java.time.LocalDateTime.parse("2018-03-21T16:00:00");
        java.sql.Timestamp tsMod = java.sql.Timestamp.valueOf(ldtMod);

        tblGridmeasure.setModDate(tsMod);
        org.junit.Assert.assertEquals(tsMod, tblGridmeasure.getModDate());
    }


}
