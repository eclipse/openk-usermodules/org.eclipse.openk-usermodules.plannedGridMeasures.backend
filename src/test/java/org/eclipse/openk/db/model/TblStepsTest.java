/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TblStepsTest {



    @Test
    public void testGettersAndSetters() {

        TblSteps tblSteps = new TblSteps();

        tblSteps.setId(1);
        org.junit.Assert.assertEquals((Integer)1, tblSteps.getId());

        tblSteps.setSortorder(2);
        assertEquals((Integer)2, tblSteps.getSortorder());

        tblSteps.setSwitchingObject("tool");
        assertEquals("tool", tblSteps.getSwitchingObject());

        tblSteps.setType("type");
        assertEquals("type", tblSteps.getType());

        tblSteps.setPresentTime("presentTime");
        assertEquals("presentTime", tblSteps.getPresentTime());

        tblSteps.setPresentState("presentState");
        assertEquals("presentState", tblSteps.getPresentState());

        tblSteps.setOperator("operator");
        assertEquals("operator", tblSteps.getOperator());

        tblSteps.setTargetState("target");
        assertEquals("target", tblSteps.getTargetState());

        tblSteps.setFkTblSingleGridmeasure(4);
        assertEquals((Integer)4, tblSteps.getFkTblSingleGridmeasure());

        tblSteps.setCreateUser("Create User");
        assertEquals("Create User", tblSteps.getCreateUser());

        java.time.LocalDateTime ldtCreate = java.time.LocalDateTime.parse("2018-03-20T15:00:00");
        java.sql.Timestamp tsCreate = java.sql.Timestamp.valueOf(ldtCreate);

        tblSteps.setCreateDate(tsCreate);
        org.junit.Assert.assertEquals(tsCreate, tblSteps.getCreateDate());

        tblSteps.setModUser("Modifiing User");
        assertEquals("Modifiing User", tblSteps.getModUser());

        java.time.LocalDateTime ldtMod = java.time.LocalDateTime.parse("2018-03-21T16:00:00");
        java.sql.Timestamp tsMod = java.sql.Timestamp.valueOf(ldtMod);

        tblSteps.setModDate(tsMod);
        org.junit.Assert.assertEquals(tsMod, tblSteps.getModDate());
    }


}
