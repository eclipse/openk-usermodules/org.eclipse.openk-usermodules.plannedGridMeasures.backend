/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class RefBranchLevelTest {

    @Test
    public void testGettersAndSetters() {

        RefBranchLevel refBranchLevel = new RefBranchLevel();

        refBranchLevel.setId(5);
        assertEquals((Integer)5, refBranchLevel.getId());

        refBranchLevel.setName("BranchLevelName");
        assertEquals("BranchLevelName", refBranchLevel.getName());

        refBranchLevel.setDescription("Description");
        assertEquals("Description", refBranchLevel.getDescription());

        refBranchLevel.setFkRefBranch(2);
        assertEquals((Integer)2, refBranchLevel.getFkRefBranch());

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-04-20T16:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        refBranchLevel.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, refBranchLevel.getCreateDate());

        refBranchLevel.setCreateUser("Carly Creator");
        assertEquals("Carly Creator", refBranchLevel.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-04-21T17:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        refBranchLevel.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, refBranchLevel.getModDate());

        refBranchLevel.setModUser("Mauro Modifier");
        assertEquals("Mauro Modifier", refBranchLevel.getModUser());

    }
}
