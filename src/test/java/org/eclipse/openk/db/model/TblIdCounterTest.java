/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;


public class TblIdCounterTest {

    @Test
    public void testGettersAndSetters() {

        TblIdCounter idCounter = new TblIdCounter();

        idCounter.setId(3);
        assertEquals((Integer)3, idCounter.getId());

        idCounter.setCounter(54);
        assertEquals((Integer)54, idCounter.getCounter());

        idCounter.setCounterType("big");
        assertEquals("big", idCounter.getCounterType());

        idCounter.setModifiedDate("2018-04-20T16:00:00");
        assertEquals("2018-04-20T16:00:00", idCounter.getModifiedDate());

        idCounter.setCreateUser("user");
        assertEquals("user", idCounter.getCreateUser());

        LocalDateTime ldtCreated = LocalDateTime.parse("2018-04-20T16:00:00");
        Timestamp tsCreated = Timestamp.valueOf(ldtCreated);
        idCounter.setCreateDate(tsCreated);
        Assert.assertEquals(tsCreated, idCounter.getCreateDate());


        LocalDateTime ldtModified = LocalDateTime.parse("2018-04-21T17:00:00");
        Timestamp tsModified = Timestamp.valueOf(ldtModified);
        idCounter.setModDate(tsModified);
        Assert.assertEquals(tsModified, idCounter.getModDate());

        idCounter.setModUser("Max Modifier");
        assertEquals("Max Modifier", idCounter.getModUser());

    }
}
