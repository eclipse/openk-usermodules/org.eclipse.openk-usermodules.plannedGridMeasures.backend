/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class RefTerritoryTest {

    @Test
    public void testGettersAndSetters() {

        RefTerritory refTerritory = new RefTerritory();

        refTerritory.setId(1);
        assertEquals((Integer)1, refTerritory.getId());

        refTerritory.setName("hugo");
        assertEquals("hugo", refTerritory.getName());

        refTerritory.setDescription("description");
        assertEquals("description", refTerritory.getDescription());

        refTerritory.setCreateUser("Create User");
        assertEquals("Create User", refTerritory.getCreateUser());

        java.time.LocalDateTime ldtCreate = java.time.LocalDateTime.parse("2018-08-20T15:00:00");
        java.sql.Timestamp tsCreate = java.sql.Timestamp.valueOf(ldtCreate);

        refTerritory.setCreateDate(tsCreate);
        org.junit.Assert.assertEquals(tsCreate, refTerritory.getCreateDate());

        refTerritory.setModUser("Modifiing User");
        assertEquals("Modifiing User", refTerritory.getModUser());

        java.time.LocalDateTime ldtMod = java.time.LocalDateTime.parse("2018-08-21T16:00:00");
        java.sql.Timestamp tsMod = java.sql.Timestamp.valueOf(ldtMod);

        refTerritory.setModDate(tsMod);
        org.junit.Assert.assertEquals(tsMod, refTerritory.getModDate());
    }

}

