/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.db.model;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class RefGmStatusTest {

    @Test
    public void testGettersAndSetters() {

        RefGmStatus refGmStatus = new RefGmStatus();

        refGmStatus.setId(5);
        assertEquals((Integer)5, refGmStatus.getId());

        refGmStatus.setName("Statusname");
        assertEquals("Statusname", refGmStatus.getName());

        refGmStatus.setColorCode("#111111");
        assertEquals("#111111", refGmStatus.getColorCode());

        java.time.LocalDateTime ldtCreated = java.time.LocalDateTime.parse("2018-03-20T15:00:00");
        java.sql.Timestamp tsCreated = java.sql.Timestamp.valueOf(ldtCreated);
        refGmStatus.setCreateDate(tsCreated);
        org.junit.Assert.assertEquals(tsCreated, refGmStatus.getCreateDate());

        refGmStatus.setCreateUser("Conny Creator");
        assertEquals("Conny Creator", refGmStatus.getCreateUser());

        java.time.LocalDateTime ldtModified = java.time.LocalDateTime.parse("2018-03-21T15:00:00");
        java.sql.Timestamp tsModified = java.sql.Timestamp.valueOf(ldtModified);
        refGmStatus.setModDate(tsModified);
        org.junit.Assert.assertEquals(tsModified, refGmStatus.getModDate());

        refGmStatus.setModUser("Mara Modifier");
        assertEquals("Mara Modifier", refGmStatus.getModUser());

    }
}
