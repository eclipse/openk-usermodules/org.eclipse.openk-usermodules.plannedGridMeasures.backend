/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk;

import static org.easymock.EasyMock.anyBoolean;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.replay;

import com.codahale.metrics.health.HealthCheckRegistry;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.jetty.setup.ServletEnvironment;
import io.dropwizard.setup.Environment;
import javax.servlet.FilterRegistration;
import org.easymock.EasyMock;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class PlannedGridMeasuresApplicationTest {

    @Test
    public void testGetName() {
        assertEquals( "PlannedGridMeasures", new PlannedGridMeasuresApplication().getName());
    }

    @Test
    public void testRun() {

        PlannedGridMeasuresConfiguration conf = createConfiguration();
        PlannedGridMeasuresApplication mca = new PlannedGridMeasuresApplication();
        mca.run(conf, createMockedEnvironment());

    }

    @Test
    public void testRun2() throws Exception {
        PlannedGridMeasuresApplication mca = new PlannedGridMeasuresApplication();
        Whitebox.invokeMethod(mca, "configureCors", createMockedEnvironment() );
    }

    @Test
    public void testInitialize() {
        new PlannedGridMeasuresApplication().initialize(null);
    }

    private PlannedGridMeasuresConfiguration createConfiguration() {
        PlannedGridMeasuresConfiguration c = new PlannedGridMeasuresConfiguration();
        c.setDbConn(new PlannedGridMeasuresConfiguration.DBConnection());
        return c;
    }

    private Environment createMockedEnvironment() {
        FilterRegistration.Dynamic cors = EasyMock.createMock(FilterRegistration.Dynamic.class);
        expect(cors.setInitParameter(anyString(), anyString())).andReturn( Boolean.TRUE ).anyTimes();
        cors.addMappingForUrlPatterns(anyObject(), anyBoolean(), anyString());
        expectLastCall();
        replay( cors );

        ServletEnvironment se = EasyMock.createMock(ServletEnvironment.class);
        expect(se.addFilter(eq("CORS"), eq(CrossOriginFilter.class))).andReturn(cors).anyTimes();
        replay(se);

        JerseyEnvironment je = EasyMock.createMock(JerseyEnvironment.class);
        je.register(anyObject());
        expectLastCall();

        HealthCheckRegistry hcr = EasyMock.createMock(HealthCheckRegistry.class);
        hcr.register(anyString(), anyObject());
        expectLastCall();

        Environment environment = EasyMock.createMock( Environment.class );
        expect( environment.servlets() ).andReturn(se).anyTimes();
        expect( environment.jersey() ).andReturn(je).anyTimes();
        expect( environment.healthChecks() ).andReturn(hcr).anyTimes();
        replay( environment );
        verify( environment );
        return environment;
    }
}
