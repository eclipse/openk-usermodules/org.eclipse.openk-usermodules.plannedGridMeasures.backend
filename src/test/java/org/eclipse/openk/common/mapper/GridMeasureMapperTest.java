/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.common.mapper;

import org.eclipse.openk.api.GridMeasure;
import org.eclipse.openk.api.PowerSystemResource;
import org.eclipse.openk.api.SingleGridmeasure;
import org.eclipse.openk.db.model.TblGridMeasure;
import org.eclipse.openk.db.model.TblSingleGridmeasure;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

public class GridMeasureMapperTest {

    TblGridMeasure mGm = new TblGridMeasure();
    GridMeasure vmGm = new GridMeasure();
    TblSingleGridmeasure mSGm = new TblSingleGridmeasure();
    SingleGridmeasure vmSGm = new SingleGridmeasure();

    java.time.LocalDateTime ldtAppointmenStartdate;
    java.sql.Timestamp tsAppointmenStartdate;

    java.time.LocalDateTime ldtPlannedStarttimeFirstSequence;
    java.sql.Timestamp tsPlannedStarttimeFirstSequence;

    java.time.LocalDateTime ldtPlannedStarttimeFirstSinglemeasure;
    java.sql.Timestamp tsPlannedStarttimeFirstSinglemeasure;

    java.time.LocalDateTime ldtPlannedEndtimeLastSinglemeasure;
    java.sql.Timestamp tsPlannedEndtimeLastSinglemeasure;

    java.time.LocalDateTime ldtPlannedEndtimeGridmeasure;
    java.sql.Timestamp tsPlannedEndtimeGridmeasure;

    java.time.LocalDateTime ldtStarttimeFirstSequence;
    java.sql.Timestamp tsStarttimeFirstSequence;

    java.time.LocalDateTime ldtStarttimeFirstSinglemeasure;
    java.sql.Timestamp tsStarttimeFirstSinglemeasure;

    java.time.LocalDateTime ldtEndtimeLastSinglemeasure;
    java.sql.Timestamp tsEndtimeLastSinglemeasure;

    java.time.LocalDateTime ldtEndtimeGridmeasure;
    java.sql.Timestamp tsEndtimeGridmeasure;

    java.time.LocalDateTime ldtTimeOfReallocation;
    String tsTimeOfReallocation;

    java.time.LocalDateTime ldtPlannedEndtimeSinglemeasure;
    java.sql.Timestamp tsPlannedEndtimeSinglemeasure;

    java.time.LocalDateTime ldtPlannedStarttimeSinglemeasure;
    java.sql.Timestamp tsPlannedStarttimeSinglemeasure;

    @Before
    public void setDateTimeValues() {
        ldtAppointmenStartdate = java.time.LocalDateTime.parse("2018-04-10T08:00:00");
        tsAppointmenStartdate = java.sql.Timestamp.valueOf(ldtAppointmenStartdate);

        ldtPlannedStarttimeFirstSequence = java.time.LocalDateTime.parse("2018-04-11T08:00:00");
        tsPlannedStarttimeFirstSequence = java.sql.Timestamp.valueOf(ldtPlannedStarttimeFirstSequence);

        ldtPlannedStarttimeFirstSinglemeasure = java.time.LocalDateTime.parse("2018-04-12T08:00:00");
        tsPlannedStarttimeFirstSinglemeasure = java.sql.Timestamp.valueOf(ldtPlannedStarttimeFirstSinglemeasure);

        ldtPlannedEndtimeLastSinglemeasure = java.time.LocalDateTime.parse("2018-04-13T08:00:00");
        tsPlannedEndtimeLastSinglemeasure = java.sql.Timestamp.valueOf(ldtPlannedEndtimeLastSinglemeasure);

        ldtPlannedEndtimeGridmeasure = java.time.LocalDateTime.parse("2018-04-14T08:00:00");
        tsPlannedEndtimeGridmeasure = java.sql.Timestamp.valueOf(ldtPlannedEndtimeGridmeasure);

        ldtStarttimeFirstSequence = java.time.LocalDateTime.parse("2018-04-15T08:00:00");
        tsStarttimeFirstSequence = java.sql.Timestamp.valueOf(ldtStarttimeFirstSequence);

        ldtStarttimeFirstSinglemeasure = java.time.LocalDateTime.parse("2018-04-16T08:00:00");
        tsStarttimeFirstSinglemeasure = java.sql.Timestamp.valueOf(ldtStarttimeFirstSinglemeasure);

        ldtEndtimeLastSinglemeasure = java.time.LocalDateTime.parse("2018-04-17T08:00:00");
        tsEndtimeLastSinglemeasure = java.sql.Timestamp.valueOf(ldtEndtimeLastSinglemeasure);

        ldtEndtimeGridmeasure = java.time.LocalDateTime.parse("2018-04-18T08:00:00");
        tsEndtimeGridmeasure = java.sql.Timestamp.valueOf(ldtEndtimeGridmeasure);

        ldtTimeOfReallocation = java.time.LocalDateTime.parse("2018-04-19T08:00:00");
        tsTimeOfReallocation = "2018-04-19T08:00:00";

        ldtEndtimeGridmeasure = java.time.LocalDateTime.parse("2018-04-18T08:00:00");
        tsEndtimeGridmeasure = java.sql.Timestamp.valueOf(ldtEndtimeGridmeasure);

        ldtPlannedEndtimeSinglemeasure = java.time.LocalDateTime.parse("2018-04-18T08:00:00");
        tsPlannedEndtimeSinglemeasure = java.sql.Timestamp.valueOf(ldtEndtimeGridmeasure);

        ldtPlannedStarttimeSinglemeasure = java.time.LocalDateTime.parse("2018-04-18T08:00:00");
        tsPlannedStarttimeSinglemeasure = java.sql.Timestamp.valueOf(ldtEndtimeGridmeasure);
    }

    @Before
    public void defineModel() {
        mGm.setId(4);
        mGm.setIdDescriptive("20");
        mGm.setTitle("Title");
        mGm.setAffectedResource("Affected Resource");
        mGm.setRemark("Remark");
        mGm.setSwitchingObject("Transformator");
        mGm.setCostCenter("centre di costa brava");
        mGm.setApprovalBy("Armin Approver");
        mGm.setAreaOfSwitching("Northeast");
        mGm.setAppointmentRepetition("monthly");
        mGm.setAppointmentStartdate(tsAppointmenStartdate);
        mGm.setAppointmentNumberOf(6);
        mGm.setPlannedStarttimeFirstSinglemeasure(tsPlannedStarttimeFirstSinglemeasure);
        mGm.setEndtimeGridmeasure(tsEndtimeGridmeasure);
        mGm.setTimeOfReallocation(tsTimeOfReallocation);
        mGm.setDescription("This is a description");
        mGm.setFkRefBranch(2);
        mGm.setFkRefBranchLevel(3);
        mGm.setCreateUser("Create User");
        mGm.setCreateUserDepartment("Create User Department");
        mGm.setFkRefGmStatus(2);

        mSGm.setId(1);
        mSGm.setDescription("description");
        mSGm.setFkTblGridmeasure(4);
        mSGm.setSortorder(1);
        mSGm.setSwitchingObject("transformator");
        mSGm.setTitle("title");
        mSGm.setCimDescription("description");
        mSGm.setCimId("2");
        mSGm.setCimName("tool");
        mSGm.setCreateUser("otto");
        mSGm.setModUser("hugo");
        mSGm.setPlannedEndtimeSinglemeasure(tsPlannedEndtimeSinglemeasure);
        mSGm.setPlannedStarttimeSinglemeasure(tsPlannedStarttimeSinglemeasure);
    }

    @Before
    public void defineViewModel() {
        vmGm.setId(4);
        vmGm.setDescriptiveId("20");
        vmGm.setTitle("Title");
        vmGm.setAffectedResource("Affected Resource");
        vmGm.setRemark("Remark");
        vmGm.setSwitchingObject("Transformator");
        vmGm.setCostCenter("centre di costa brava");
        vmGm.setApprovalBy("Armin Approver");
        vmGm.setAreaOfSwitching("Northeast");
        vmGm.setAppointmentRepetition("monthly");
        vmGm.setAppointmentStartdate(tsAppointmenStartdate);
        vmGm.setAppointmentNumberOf(6);
        vmGm.setPlannedStarttimeFirstSinglemeasure(tsPlannedStarttimeFirstSinglemeasure);
        vmGm.setEndtimeGridmeasure(tsEndtimeGridmeasure);
        vmGm.setTimeOfReallocation(tsTimeOfReallocation);
        vmGm.setDescription("This is a description");
        vmGm.setBranchId(2);
        vmGm.setBranchLevelId(3);
        vmGm.setCreateUser("Create User");
        vmGm.setCreateUserDepartment("Create User Department");
        vmGm.setStatusId(2);

        vmSGm.setId(1);
        vmSGm.setDescription("description");
        vmSGm.setSortorder(1);
        vmSGm.setSwitchingObject("transformator");
        vmSGm.setTitle("title");
        vmSGm.setPlannedEndtimeSinglemeasure(tsPlannedEndtimeSinglemeasure);
        vmSGm.setPlannedStarttimeSinglemeasure(tsPlannedStarttimeSinglemeasure);
        PowerSystemResource pr = new PowerSystemResource();
        pr.setCimId("2");
        pr.setCimDescription("description");
        pr.setCimName("tool");
        vmSGm.setPowerSystemResource(pr);
    }

    @Test
    public void testMapFromVModel()throws IOException {

        GridMeasureMapper mapper = new GridMeasureMapper();
        TblGridMeasure testmodel =  mapper.mapFromVModel(vmGm);

        assertEquals(mGm.getId(), testmodel.getId());
        assertEquals(mGm.getIdDescriptive(), testmodel.getIdDescriptive());
        assertEquals(mGm.getTitle(), testmodel.getTitle());
        assertEquals(mGm.getAffectedResource(), testmodel.getAffectedResource());
        assertEquals(mGm.getRemark(), testmodel.getRemark());
        assertEquals(mGm.getSwitchingObject(), testmodel.getSwitchingObject());
        assertEquals(mGm.getCostCenter(), testmodel.getCostCenter());
        assertEquals(mGm.getApprovalBy(), testmodel.getApprovalBy());
        assertEquals(mGm.getAreaOfSwitching(), testmodel.getAreaOfSwitching());
        assertEquals(mGm.getAppointmentRepetition(), testmodel.getAppointmentRepetition());
        assertEquals(mGm.getAppointmentStartdate(), testmodel.getAppointmentStartdate());
        assertEquals(mGm.getAppointmentNumberOf(), testmodel.getAppointmentNumberOf());
        assertEquals(mGm.getPlannedStarttimeFirstSinglemeasure(), testmodel.getPlannedStarttimeFirstSinglemeasure());
        assertEquals(mGm.getEndtimeGridmeasure(), testmodel.getEndtimeGridmeasure());
        assertEquals(mGm.getTimeOfReallocation(), testmodel.getTimeOfReallocation());
        assertEquals(mGm.getDescription(), testmodel.getDescription());
        assertEquals(mGm.getFkRefBranch(), testmodel.getFkRefBranch());
        assertEquals(mGm.getFkRefBranchLevel(), testmodel.getFkRefBranchLevel());
        assertEquals(mGm.getCreateUser(), testmodel.getCreateUser());
        assertEquals(mGm.getCreateUserDepartment(), testmodel.getCreateUserDepartment());

    }

    @Test
    public void testMapFromModel()throws IOException{

        GridMeasureMapper mapper = new GridMeasureMapper();
        GridMeasure testVmodel =  mapper.mapToVModel(mGm);

        assertEquals(vmGm.getId(), testVmodel.getId());
        assertEquals(vmGm.getDescriptiveId(), testVmodel.getDescriptiveId());
        assertEquals(vmGm.getTitle(), testVmodel.getTitle());
        assertEquals(vmGm.getAffectedResource(), testVmodel.getAffectedResource());
        assertEquals(vmGm.getRemark(), testVmodel.getRemark());
        assertEquals(vmGm.getSwitchingObject(), testVmodel.getSwitchingObject());
        assertEquals(vmGm.getCostCenter(), testVmodel.getCostCenter());
        assertEquals(vmGm.getApprovalBy(), testVmodel.getApprovalBy());
        assertEquals(vmGm.getAreaOfSwitching(), testVmodel.getAreaOfSwitching());
        assertEquals(vmGm.getAppointmentRepetition(), testVmodel.getAppointmentRepetition());
        assertEquals(vmGm.getAppointmentStartdate(), testVmodel.getAppointmentStartdate());
        assertEquals(vmGm.getAppointmentNumberOf(), testVmodel.getAppointmentNumberOf());
        assertEquals(vmGm.getPlannedStarttimeFirstSinglemeasure(), testVmodel.getPlannedStarttimeFirstSinglemeasure());
        assertEquals(vmGm.getEndtimeGridmeasure(), testVmodel.getEndtimeGridmeasure());
        assertEquals(vmGm.getTimeOfReallocation(), testVmodel.getTimeOfReallocation());
        assertEquals(vmGm.getDescription(), testVmodel.getDescription());
        assertEquals(vmGm.getBranchId(), testVmodel.getBranchId());
        assertEquals(vmGm.getBranchLevelId(), testVmodel.getBranchLevelId());
        assertEquals(vmGm.getCreateUser(), testVmodel.getCreateUser());
        assertEquals(vmGm.getCreateUserDepartment(), testVmodel.getCreateUserDepartment());
    }

    @Test
    public void testAdjustPowerSystemResourceMapFromVModel()throws IOException {

        GridMeasureMapper mapper = new GridMeasureMapper();
        mapper.adjustPowerSystemResourceMapFromViewModel(vmSGm, mSGm);

        assertEquals(mSGm.getCimId(), vmSGm.getPowerSystemResource().getCimId());
        assertEquals(mSGm.getCimDescription(), vmSGm.getPowerSystemResource().getCimDescription());
        assertEquals(mSGm.getCimName(), vmSGm.getPowerSystemResource().getCimName());
    }


}
