/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.common.mapper.generic;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotSame;

import java.sql.Date;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelClass;
import org.eclipse.openk.common.mapper.generic.annotations.MapDbModelField;
import org.junit.Test;

class TestPojoDb {
    private int integerField;
    private String stringField;
    private Date dateField;
    private boolean bField;
    private byte byField;
    private char charField;
    private double doField;
    private float floField;
    private long loField;
    private short shorField;

    private String independedField;

    public TestPojoDb() {

    }

    public int getIntegerField() {
        return integerField;
    }

    public void setIntegerField(int integerField) {
        this.integerField = integerField;
    }

    public String getStringField() {
        return stringField;
    }

    public void setStringField(String stringField) {
        this.stringField = stringField;
    }

    public Date getDateField() {
        return dateField;
    }

    public void setDateField(Date dateField) {
        this.dateField = dateField;
    }

    public boolean isbField() {
        return bField;
    }

    public void setbField(boolean bField) {
        this.bField = bField;
    }

    public byte getByField() {
        return byField;
    }

    public void setByField(byte byField) {
        this.byField = byField;
    }

    public char getCharField() {
        return charField;
    }

    public void setCharField(char charField) {
        this.charField = charField;
    }

    public double getDoField() {
        return doField;
    }

    public void setDoField(double doField) {
        this.doField = doField;
    }

    public float getFloField() {
        return floField;
    }

    public void setFloField(float floField) {
        this.floField = floField;
    }

    public long getLoField() {
        return loField;
    }

    public void setLoField(long loField) {
        this.loField = loField;
    }

    public short getShorField() {
        return shorField;
    }

    public void setShorField(short shorField) {
        this.shorField = shorField;
    }

    public String getIndependedField() {
        return independedField;
    }

    public void setIndependedField(String independedField) {
        this.independedField = independedField;
    }
}

class TestClassWithoutAnnotation {
    private int i;
}

@MapDbModelClass(classType = GenericApiToDbMapper.class)
class TestClassWithWrongAnnotation {
    private float foo;
}



@MapDbModelClass(classType = TestPojoDb.class)
class TestPojoVMWithoutConstructor {
    private TestPojoVMWithoutConstructor() {
    }
}


@MapDbModelClass(classType = TestPojoDb.class)
class TestPojoVM {
    public TestPojoVM() {}

    @MapDbModelField( fieldName = "integerField")
    private int intField;

    @MapDbModelField( fieldName = "stringField")
    private String strField;

    @MapDbModelField
    private Date dateField;

    @MapDbModelField
    private boolean bField;

    @MapDbModelField
    private byte byField;

    @MapDbModelField
    private char charField;

    @MapDbModelField
    private double doField;

    @MapDbModelField
    private float floField;

    @MapDbModelField
    private long loField;

    @MapDbModelField
    private short shorField;

    private String independedField;

    public int getIntField() {
        return intField;
    }

    public void setIntegerField(int integerField) {
        this.intField = integerField;
    }

    public Date getDateField() {
        return dateField;
    }

    public void setDateField(Date dateField) {
        this.dateField = dateField;
    }

    public void setIntField(int intField) {
        this.intField = intField;
    }

    public String getStrField() {
        return strField;
    }

    public void setStrField(String strField) {
        this.strField = strField;
    }

    public boolean isbField() {
        return bField;
    }

    public void setbField(boolean bField) {
        this.bField = bField;
    }

    public byte getByField() {
        return byField;
    }

    public void setByField(byte byField) {
        this.byField = byField;
    }

    public char getCharField() {
        return charField;
    }

    public void setCharField(char charField) {
        this.charField = charField;
    }

    public double getDoField() {
        return doField;
    }

    public void setDoField(double doField) {
        this.doField = doField;
    }

    public float getFloField() {
        return floField;
    }

    public void setFloField(float floField) {
        this.floField = floField;
    }

    public long getLoField() {
        return loField;
    }

    public void setLoField(long loField) {
        this.loField = loField;
    }

    public short getShorField() {
        return shorField;
    }

    public void setShorField(short shorField) {
        this.shorField = shorField;
    }

    public String getIndependedField() {
        return independedField;
    }

    public void setIndependedField(String independedField) {
        this.independedField = independedField;
    }
}


public class GenericApiToDBMapperTest {

    private TestPojoDb fillDBPojo() {
        TestPojoDb p = new TestPojoDb();
        p.setDateField(new java.sql.Date(System.currentTimeMillis()));
        p.setIntegerField(666);
        p.setStringField("Der Testat");
        p.setbField(true);
        p.setByField((byte)255);
        p.setCharField('f');
        p.setDoField(1.2f);
        p.setFloField(1.3f);
        p.setLoField(1024L);
        p.setShorField((short)640);

        p.setIndependedField("DBOwn");

        return p;
    }

    @Test
    public void testMapper() throws Exception {
        TestPojoDb db = fillDBPojo();
        db.setIndependedField("Independence");
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();

        TestPojoVM vmpojo = mapper.mapToViewModel(TestPojoVM.class, db);

        assertEquals( db.getIntegerField(), vmpojo.getIntField());
        assertEquals( db.getStringField(), vmpojo.getStrField());
        assertEquals( db.isbField(), vmpojo.isbField());
        assertEquals( db.getByField(), vmpojo.getByField());
        assertEquals( db.getCharField(), vmpojo.getCharField());
        assertEquals( db.getDoField(), vmpojo.getDoField());
        assertEquals( db.getFloField(), vmpojo.getFloField());
        assertEquals( db.getLoField(), vmpojo.getLoField());
        assertEquals( db.getShorField(), vmpojo.getShorField());

        TestPojoDb dbpojo = mapper.mapFromViewModel(TestPojoDb.class, vmpojo);

        assertEquals( db.getIntegerField(), dbpojo.getIntegerField());
        assertEquals( db.getStringField(), dbpojo.getStringField());
        assertEquals( db.isbField(), dbpojo.isbField());
        assertEquals( db.getByField(), dbpojo.getByField());
        assertEquals( db.getCharField(), dbpojo.getCharField());
        assertEquals( db.getDoField(), dbpojo.getDoField());
        assertEquals( db.getFloField(), dbpojo.getFloField());
        assertEquals( db.getLoField(), dbpojo.getLoField());
        assertEquals( db.getShorField(), dbpojo.getShorField());

        assertNotSame( db.getIndependedField(), dbpojo.getIndependedField());

    }

    @Test( expected = Exception.class )
    public void testMapperWithMissingClassAnno() throws Exception {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        TestClassWithoutAnnotation testObj = new TestClassWithoutAnnotation();
        TestPojoDb dbpojo = mapper.mapFromViewModel(TestPojoDb.class, testObj);
    }

    @Test( expected = Exception.class )
    public void testMapperWithWrongClassAnno() throws Exception {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        TestClassWithWrongAnnotation testObj = new TestClassWithWrongAnnotation();
        TestPojoDb dbpojo = mapper.mapToViewModel(TestPojoDb.class, testObj);
    }


    @Test( expected = Exception.class )
    public void testMapperWithNoConstrClass() throws Exception {
        GenericApiToDbMapper mapper = new GenericApiToDbMapper();
        TestPojoDb dbpojo = new TestPojoDb();

        TestPojoVMWithoutConstructor target = mapper.mapToViewModel(TestPojoVMWithoutConstructor.class, dbpojo);
    }
}
