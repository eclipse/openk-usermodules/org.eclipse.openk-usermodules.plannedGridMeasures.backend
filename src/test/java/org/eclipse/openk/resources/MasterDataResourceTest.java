/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.resources;

import org.easymock.EasyMock;
import org.eclipse.openk.TestUtils.TestHelper;
import org.eclipse.openk.api.GmStatus;
import org.eclipse.openk.api.UserSettings;
import org.eclipse.openk.core.controller.MasterDataBackendController;
import org.eclipse.openk.core.controller.TokenManager;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.easymock.EasyMock.expect;

public class MasterDataResourceTest {

    GmStatus gmStatus = new GmStatus();

    @Before
    public void createGridMeasure() {
        TestHelper.initDefaultBackendConfig();
        TokenManager.getInstance();
    }

    @Test
    public void testGetGmStatus() {
        Response r = new MasterDataResource().getGmStatus("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetBranches() {
        Response r = new MasterDataResource().getBranches("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetBranchLevels() {
        Response r = new MasterDataResource().getBranchLevelsByBranch("LET_ME_IN", "1");
        assertNotNull( r );
    }

    @Test
    public void testGetCostCenters() {
        Response r = new MasterDataResource().getCostCenters("LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGeTerritories() {
        Response r = new MasterDataResource().getTerritories("LET_ME_IN");
        assertNotNull( r );
    }


    @Test
    public void testStoreUserSettings_update() throws HttpStatusException{

        UserSettings us = createUserSetting();

        MasterDataBackendController bc = EasyMock.createMock(MasterDataBackendController.class);
        expect(bc.storeUserSettings("Ulli User", us)).andReturn(us).anyTimes();

        EasyMock.replay(bc);
        EasyMock.verify(bc);

        UserSettings us2 = bc.storeUserSettings("Ulli User", us);
        assertEquals(us, us2);
    }

    @Test
    public void testStoreUserSettings_create() throws HttpStatusException{

        UserSettings us = createUserSetting();
        UserSettings us2 = createUserSetting();

        MasterDataBackendController bc = EasyMock.createMock(MasterDataBackendController.class);
        expect(bc.storeUserSettings("Ulli User", us)).andReturn(us2).anyTimes();

        EasyMock.replay(bc);
        EasyMock.verify(bc);

        UserSettings us3 = bc.storeUserSettings("Ulli User", us);
    }

    public UserSettings createUserSetting()
    {
        UserSettings userSetting = new UserSettings();
        userSetting.setSettingType("yyy");
        userSetting.setUsername("Ulli User");
        userSetting.setValue("");

        return userSetting;
    }

    @Test
    public void testGetUserSettings() {
        Response r = new MasterDataResource().getUserSettings("xxx", "LET_ME_IN");
        assertNotNull( r );
    }

    @Test
    public void testGetEmailAddressesFromTemplates() {
        Response r = new MasterDataResource().getEmailAddressesFromTemplates("LET_ME_IN");
        assertNotNull( r );
    }




}
