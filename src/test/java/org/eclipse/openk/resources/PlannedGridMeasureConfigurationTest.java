/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.resources;

import static org.junit.Assert.assertEquals;

import org.eclipse.openk.PlannedGridMeasuresConfiguration;
import org.junit.Test;

public class PlannedGridMeasureConfigurationTest {

    @Test
    public void testGettersAndSetters() {

        PlannedGridMeasuresConfiguration plannedGridMeasuresConfiguration = new PlannedGridMeasuresConfiguration();

        PlannedGridMeasuresConfiguration.DBConnection dbConn = new PlannedGridMeasuresConfiguration.DBConnection();
        dbConn.setUrl("url");
        dbConn.setDriver("driver");
        dbConn.setPassword("strong");
        dbConn.setUser("rambo");

        // test db connection
        plannedGridMeasuresConfiguration.setDbConn(dbConn);
        assertEquals(dbConn, plannedGridMeasuresConfiguration.getDbConn());

        // test db properties
        assertEquals("url", dbConn.getUrl());
        assertEquals("driver", dbConn.getDriver());
        assertEquals("strong", dbConn.getPassword());
        assertEquals("rambo", dbConn.getUser());

        // test persistencyUnit
        plannedGridMeasuresConfiguration.setPersistencyUnit("planned-grid-measures");
        assertEquals("planned-grid-measures", plannedGridMeasuresConfiguration.getPersistencyUnit());

        // test PortalBaseURL
        plannedGridMeasuresConfiguration.setPortalBaseURL("http://localhost:8080/portal/rest/beservice");
        assertEquals("http://localhost:8080/portal/rest/beservice", plannedGridMeasuresConfiguration.getPortalBaseURL());

        // test WhiteListDocumenttypes
        plannedGridMeasuresConfiguration.setWhiteListDocumenttypes("doc xls");
        assertEquals("doc xls", plannedGridMeasuresConfiguration.getWhiteListDocumenttypes());

    }
}
