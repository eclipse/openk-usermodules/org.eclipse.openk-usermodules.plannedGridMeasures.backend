/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.health;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import com.codahale.metrics.health.HealthCheck;
import org.eclipse.openk.api.VersionInfo;
import org.junit.Test;

public class DBIsPresentHealthCheckTest {
    class MockHC extends DBIsPresentHealthCheck {
        public String dbversion;
        public String beversion;
        public boolean throwException;
        public boolean provideNull;

        protected VersionInfo getVersionInfoFromResource()  {
            if(throwException) {
                throw new RuntimeException("As demanded!");
            }
            if(provideNull)
            {
                return null;
            }
            VersionInfo vi = new VersionInfo();
            vi.setDbVersion(dbversion);
            vi.setBackendVersion(beversion);
            return vi;
        }
    }

    @Test
    public void testCheck() throws Exception {
        MockHC hc = new MockHC();
        hc.beversion="1_BE";
        hc.dbversion="2_DB";
        hc.throwException=false;

        HealthCheck.Result r = hc.check();
        assertTrue( r.isHealthy() );

        hc.beversion="";
        assertFalse( hc.check().isHealthy());
        assertEquals( "service not ready", hc.check().getMessage());

        hc.beversion="1_BE";
        hc.dbversion="";
        assertFalse( hc.check().isHealthy());
        hc.dbversion="NO_DB";
        assertFalse( hc.check().isHealthy());
        assertEquals( "DB not ready", hc.check().getMessage());

        hc.provideNull=true;
        assertFalse( hc.check().isHealthy());
    }

    @Test( expected = Exception.class )
    public void testCheck_Exception() throws Exception {
        MockHC hc = new MockHC();
        hc.throwException=false;
        hc.check();
    }
}
